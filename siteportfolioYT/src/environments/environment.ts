// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyD-muTcMYYnv4Ehx5VxvDzydWhsZMSsO3o",
    authDomain: "sitemycv2020.firebaseapp.com",
    databaseURL: "https://sitemycv2020.firebaseio.com",
    projectId: "sitemycv2020",
    storageBucket: "sitemycv2020.appspot.com",
    messagingSenderId: "265636062200",
    appId: "1:265636062200:web:c84f0c22841fe248976c0d",
    measurementId: "G-0N6DPWK1E4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
