import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContactComponent } from './contact/contact.component';
import { CompetenceComponent } from './competence/competence.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { HomeComponent } from './home/home.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {environment} from "../environments/environment";
import {AngularFireModule} from "@angular/fire";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import { PagedevComponent } from './pagedev/pagedev.component';
import { ExperienceComponent } from './experience/experience.component';
import { LoisirComponent } from './loisir/loisir.component';

@NgModule({
  declarations: [
    AppComponent,
    ContactComponent,
    CompetenceComponent,
    PortfolioComponent,
    HomeComponent,
    PagedevComponent,
    ExperienceComponent,
    LoisirComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
