export interface MailModel {
  name:string;
  email:string;
  object?:string;
  body:string;
}
