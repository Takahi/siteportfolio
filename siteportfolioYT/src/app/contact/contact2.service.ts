import {from} from "rxjs";
import {Injectable} from "@angular/core";
import {MailModel} from "./mail.model";
import {AngularFirestore} from "@angular/fire/firestore";

@Injectable({
  providedIn: 'root'
})

export class ContactService {
  constructor(
    private afs: AngularFirestore,
  ) {  }
  addnewmail(mail: MailModel) {
    return from(this.afs.collection('mailfromweb').add(mail))
  }

}
