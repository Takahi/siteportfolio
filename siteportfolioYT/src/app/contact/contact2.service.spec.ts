import { TestBed } from '@angular/core/testing';

import { Contact2Service } from './contact2.service';

describe('Contact2Service', () => {
  let service: Contact2Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Contact2Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
