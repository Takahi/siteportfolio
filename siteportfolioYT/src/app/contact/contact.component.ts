import {Component, } from '@angular/core';
import {FormBuilder, Validators, FormControl, FormGroup} from '@angular/forms';
import {ContactService} from './contact2.service';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})

export class ContactComponent {

  constructor(
    private fb: FormBuilder,
    private contactService: ContactService
  ) {
  }

  contactform = this.fb.group({
    name: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    object: [''],
    body: ['', Validators.required],
  });

  get nameControl() {
    return this.contactform.get('name') as FormControl;
  }

  get emailControl() {
    return this.contactform.get('email') as FormControl;
  }

  get objectControl() {
    return this.contactform.get('object') as FormControl;
  }

  get bodyControl() {
    return this.contactform.get('body') as FormControl;
  }

  viewconfirm() {
    document.getElementById("modalconfirm").style.display = "block";
  }

  sendform() {
    this.contactService.addnewmail(this.contactform.value).subscribe();
    document.getElementById("modalsended").style.display = "block";
    console.log(this.contactform.value);
  }

  closeconfirm() {
    document.getElementById("modalconfirm").style.display = "none";
  }

  closesended() {
    document.getElementById("modalsended").style.display = "none";
  }

  onEvent(e) {
    e.preventDefault();
    e.stopPropagation();
  }

}

