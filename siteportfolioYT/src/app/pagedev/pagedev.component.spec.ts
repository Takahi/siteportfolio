import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagedevComponent } from './pagedev.component';

describe('PagedevComponent', () => {
  let component: PagedevComponent;
  let fixture: ComponentFixture<PagedevComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagedevComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagedevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
