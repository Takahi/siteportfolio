import { TestBed } from '@angular/core/testing';

import { PagedevService } from './pagedev.service';

describe('PagedevService', () => {
  let service: PagedevService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PagedevService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
