import { Component, OnInit } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from "@angular/fire/firestore";
import {Observable} from "rxjs";

@Component({
  selector: 'app-pagedev',
  templateUrl: './pagedev.component.html',
  styleUrls: ['./pagedev.component.scss']
})
export class PagedevComponent {

  mails: Observable<any[]>;

  constructor(db: AngularFirestore) {
    this.mails = db.collection('mailfromweb').valueChanges();
  }
  ngOnInit(): void {
  }

}
