import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {CompetenceComponent} from "./competence/competence.component";
import {ContactComponent} from "./contact/contact.component";
import {PortfolioComponent} from "./portfolio/portfolio.component";
import {PagedevComponent} from "./pagedev/pagedev.component";
import {ExperienceComponent} from "./experience/experience.component";
import {LoisirComponent} from "./loisir/loisir.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'accueil', component: HomeComponent},
  {path: 'experience', component: ExperienceComponent},
  {path: 'loisir', component: LoisirComponent},
  {path: 'competence', component: CompetenceComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'portfolio', component: PortfolioComponent},
  {path: 'pagedev000', component: PagedevComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
