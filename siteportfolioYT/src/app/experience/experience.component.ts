import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

  ballmove() {
    var target = document.getElementById("ballerika");
    target.classList.remove('ballerikayellow');
    target.classList.add('ballerikared');
  }
}
