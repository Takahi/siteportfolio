(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _competence_competence_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./competence/competence.component */ "./src/app/competence/competence.component.ts");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./contact/contact.component */ "./src/app/contact/contact.component.ts");
/* harmony import */ var _portfolio_portfolio_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./portfolio/portfolio.component */ "./src/app/portfolio/portfolio.component.ts");
/* harmony import */ var _pagedev_pagedev_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pagedev/pagedev.component */ "./src/app/pagedev/pagedev.component.ts");
/* harmony import */ var _experience_experience_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./experience/experience.component */ "./src/app/experience/experience.component.ts");
/* harmony import */ var _loisir_loisir_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./loisir/loisir.component */ "./src/app/loisir/loisir.component.ts");











const routes = [
    { path: '', component: _home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"] },
    { path: 'accueil', component: _home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"] },
    { path: 'experience', component: _experience_experience_component__WEBPACK_IMPORTED_MODULE_7__["ExperienceComponent"] },
    { path: 'loisir', component: _loisir_loisir_component__WEBPACK_IMPORTED_MODULE_8__["LoisirComponent"] },
    { path: 'competence', component: _competence_competence_component__WEBPACK_IMPORTED_MODULE_3__["CompetenceComponent"] },
    { path: 'contact', component: _contact_contact_component__WEBPACK_IMPORTED_MODULE_4__["ContactComponent"] },
    { path: 'portfolio', component: _portfolio_portfolio_component__WEBPACK_IMPORTED_MODULE_5__["PortfolioComponent"] },
    { path: 'pagedev000', component: _pagedev_pagedev_component__WEBPACK_IMPORTED_MODULE_6__["PagedevComponent"] },
];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



class AppComponent {
    constructor() {
        this.title = 'siteportfolioYT';
        this.mail1 = "dezidezi94";
        this.mail2 = "95@outlook.com";
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 94, vars: 2, consts: [[1, "contenair"], ["type", "checkbox", "id", "openSidebarMenu"], ["for", "openSidebarMenu", 1, "sidebarIconToggle"], [1, "spinner", "diagonal", "part-1"], [1, "spinner", "horizontal"], [1, "spinner", "diagonal", "part-2"], ["id", "sidebarMenu"], [1, "sidebarMenuInner"], ["onClick", "jumptop()", "routerLink", "/accueil"], ["onClick", "jump()", "routerLink", "/experience"], ["onClick", "jump()", "routerLink", "/competence"], ["onClick", "jump()", "routerLink", "/portfolio"], ["onClick", "jump()", "routerLink", "/loisir"], ["onClick", "jump()", "routerLink", "/contact"], [1, "linkinconbox"], [1, "linkicon"], ["href", "mailto:dezidezi9495@outlook.com"], ["src", "../assets/icons8-composing-mail-100.png", "alt", "", 1, "linkimg"], ["href", "https://www.linkedin.com/in/takahiro-y-551a1918b/"], ["src", "../assets/icons8-linkedin-100.png", "alt", "", 1, "linkimg"], ["href", ""], ["src", "../assets/icons8-slack-new-100.png", "alt", "", 1, "linkimg"], ["href", "https://gitlab.com/Takahi"], ["src", "../assets/icons8-gitlab-100.png", "alt", "", 1, "linkimg"], ["href", "https://www.facebook.com/profile.php?id=100010351771579"], ["src", "../assets/icons8-facebook-old-100.png", "alt", "", 1, "linkimg"], ["href", "https://twitter.com/adreamtogalaxy"], ["src", "../assets/icons8-twitter-100.png", "alt", "", 1, "linkimg"], ["href", "https://www.instagram.com/takahoroo/"], ["src", "../assets/icons8-instagram-old-100.png", "alt", "", 1, "linkimg"], [1, "sec-top"], [1, "nameempty1"], [1, "name12"], [1, "name1"], [1, "name2"], [1, "nameempty2"], [1, "dlcv"], ["href", "../assets/202003CV%20YAMADA%20new%20.pdf", "target", "_blank"], ["src", "../assets/icons8-download-50.png", "alt", "download"], [1, "nameempty3"], [1, "name3"], [1, "whitebar"], [1, "sec-main"], [1, "footer"]], template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "section");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "input", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "label", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "ACCUEIL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "EXPERIENCE");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, " COMPETENCE ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "PORTFOLIO");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "a", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "LOISIR");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "a", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "CONTACT");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "a", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "img", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "a", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "img", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "a", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "a", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "img", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "a", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "img", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "a", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](45, "img", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "a", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](48, "img", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "section");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "h1", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "TAKAHIRO YAMADA");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "p", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "D\u00E9veloppeur web Front-end Junior");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](57, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "div", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "a", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "img", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "T\u00E9l\u00E9chargez Mon CV");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "p", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, "\u25BC Scroll \u25BC");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](65, "div", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "section");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](68, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "section");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "div", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "ul");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](74, "ACCUEIL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, "EXPERIENCE");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "a", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, "COMPETENCE");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](83, "PORTFOLIO");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "a", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](86, "LOISIR");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "a", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](89, "CONTACT");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "address");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](91);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](93, "\u00A9 2020 \u00B7 TAKAHIRO YAMADA \u00B7 ALL RIGHTS RESERVED");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](91);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", ctx.mail1, "", ctx.mail2, "");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]], styles: ["@charset \"UTF-8\";\nhtml[_ngcontent-%COMP%], body[_ngcontent-%COMP%], div[_ngcontent-%COMP%], span[_ngcontent-%COMP%], applet[_ngcontent-%COMP%], object[_ngcontent-%COMP%], iframe[_ngcontent-%COMP%], h1[_ngcontent-%COMP%], h2[_ngcontent-%COMP%], h3[_ngcontent-%COMP%], h4[_ngcontent-%COMP%], h5[_ngcontent-%COMP%], h6[_ngcontent-%COMP%], p[_ngcontent-%COMP%], blockquote[_ngcontent-%COMP%], pre[_ngcontent-%COMP%], a[_ngcontent-%COMP%], abbr[_ngcontent-%COMP%], acronym[_ngcontent-%COMP%], address[_ngcontent-%COMP%], big[_ngcontent-%COMP%], cite[_ngcontent-%COMP%], code[_ngcontent-%COMP%], del[_ngcontent-%COMP%], dfn[_ngcontent-%COMP%], em[_ngcontent-%COMP%], img[_ngcontent-%COMP%], ins[_ngcontent-%COMP%], kbd[_ngcontent-%COMP%], q[_ngcontent-%COMP%], s[_ngcontent-%COMP%], samp[_ngcontent-%COMP%], small[_ngcontent-%COMP%], strike[_ngcontent-%COMP%], strong[_ngcontent-%COMP%], sub[_ngcontent-%COMP%], sup[_ngcontent-%COMP%], tt[_ngcontent-%COMP%], var[_ngcontent-%COMP%], b[_ngcontent-%COMP%], u[_ngcontent-%COMP%], i[_ngcontent-%COMP%], center[_ngcontent-%COMP%], dl[_ngcontent-%COMP%], dt[_ngcontent-%COMP%], dd[_ngcontent-%COMP%], ol[_ngcontent-%COMP%], ul[_ngcontent-%COMP%], li[_ngcontent-%COMP%], fieldset[_ngcontent-%COMP%], form[_ngcontent-%COMP%], label[_ngcontent-%COMP%], legend[_ngcontent-%COMP%], table[_ngcontent-%COMP%], caption[_ngcontent-%COMP%], tbody[_ngcontent-%COMP%], tfoot[_ngcontent-%COMP%], thead[_ngcontent-%COMP%], tr[_ngcontent-%COMP%], th[_ngcontent-%COMP%], td[_ngcontent-%COMP%], article[_ngcontent-%COMP%], aside[_ngcontent-%COMP%], canvas[_ngcontent-%COMP%], details[_ngcontent-%COMP%], embed[_ngcontent-%COMP%], figure[_ngcontent-%COMP%], figcaption[_ngcontent-%COMP%], footer[_ngcontent-%COMP%], header[_ngcontent-%COMP%], hgroup[_ngcontent-%COMP%], menu[_ngcontent-%COMP%], nav[_ngcontent-%COMP%], output[_ngcontent-%COMP%], ruby[_ngcontent-%COMP%], section[_ngcontent-%COMP%], summary[_ngcontent-%COMP%], time[_ngcontent-%COMP%], mark[_ngcontent-%COMP%], audio[_ngcontent-%COMP%], video[_ngcontent-%COMP%] {\n  margin: 0;\n  padding: 0;\n  border: 0;\n  vertical-align: baseline;\n  font-family: \"Work Sans\", sans-serif;\n}\narticle[_ngcontent-%COMP%], aside[_ngcontent-%COMP%], details[_ngcontent-%COMP%], figcaption[_ngcontent-%COMP%], figure[_ngcontent-%COMP%], footer[_ngcontent-%COMP%], header[_ngcontent-%COMP%], hgroup[_ngcontent-%COMP%], menu[_ngcontent-%COMP%], nav[_ngcontent-%COMP%], section[_ngcontent-%COMP%] {\n  display: block;\n}\nhtml[_ngcontent-%COMP%], body[_ngcontent-%COMP%] {\n  overflow-x: hidden;\n  margin: 0;\n  padding: 0;\n  border: 0;\n}\nbody[_ngcontent-%COMP%] {\n  background: #fff;\n  padding: 0;\n  margin: 0;\n  font-family: \"Varela Round\", sans-serif;\n}\nh2[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.sidebarIconToggle[_ngcontent-%COMP%] {\n  transition: all 0.3s;\n  box-sizing: border-box;\n  cursor: pointer;\n  position: fixed;\n  width: 44px;\n  height: 44px;\n  z-index: 961;\n  top: 22px;\n  left: 20px;\n}\n.spinner[_ngcontent-%COMP%] {\n  transition: all 0.3s;\n  box-sizing: border-box;\n  position: absolute;\n  height: 6px;\n  width: 100%;\n  background-color: #fff;\n}\n.horizontal[_ngcontent-%COMP%] {\n  transition: all 0.3s;\n  box-sizing: border-box;\n  position: relative;\n  float: left;\n  margin-top: 6px;\n}\n.diagonal.part-1[_ngcontent-%COMP%] {\n  position: relative;\n  transition: all 0.3s;\n  box-sizing: border-box;\n  float: left;\n}\n.diagonal.part-2[_ngcontent-%COMP%] {\n  transition: all 0.3s;\n  box-sizing: border-box;\n  position: relative;\n  float: left;\n  margin-top: 6px;\n}\ninput[type=checkbox][_ngcontent-%COMP%]:checked    ~ #sidebarMenu[_ngcontent-%COMP%] {\n  transform: translateX(0);\n}\ninput[type=checkbox][_ngcontent-%COMP%]:checked    ~ .closebar[_ngcontent-%COMP%] {\n  display: block;\n}\ninput[type=checkbox][_ngcontent-%COMP%] {\n  transition: all 0.3s;\n  box-sizing: border-box;\n  display: none;\n}\ninput[type=checkbox][_ngcontent-%COMP%]:checked    ~ .sidebarIconToggle[_ngcontent-%COMP%]    > .horizontal[_ngcontent-%COMP%] {\n  transition: all 0.3s;\n  box-sizing: border-box;\n  opacity: 0;\n}\ninput[type=checkbox][_ngcontent-%COMP%]:checked    ~ .sidebarIconToggle[_ngcontent-%COMP%]    > .diagonal.part-1[_ngcontent-%COMP%] {\n  transition: all 0.3s;\n  box-sizing: border-box;\n  transform: rotate(135deg);\n  margin-top: 16px;\n}\ninput[type=checkbox][_ngcontent-%COMP%]:checked    ~ .sidebarIconToggle[_ngcontent-%COMP%]    > .diagonal.part-2[_ngcontent-%COMP%] {\n  transition: all 0.3s;\n  box-sizing: border-box;\n  transform: rotate(-135deg);\n  margin-top: -18px;\n}\n#sidebarMenu[_ngcontent-%COMP%] {\n  height: 100%;\n  width: 480px;\n  position: fixed;\n  left: 0;\n  margin: 0;\n  transform: translateX(-480px);\n  transition: transform 250ms ease-in-out;\n  background: rgba(0, 0, 0, 0.8);\n  z-index: 950;\n}\n.sidebarMenuInner[_ngcontent-%COMP%] {\n  margin: 70px 0 0 0;\n  border-top: 1px solid rgba(255, 255, 255, 0.1);\n}\n.sidebarMenuInner[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n  list-style: none;\n  color: #fff;\n  text-transform: uppercase;\n  font-weight: bold;\n  padding: 20px;\n  cursor: pointer;\n  border-bottom: 1px solid rgba(255, 255, 255, 0.1);\n}\n.sidebarMenuInner[_ngcontent-%COMP%]   [_ngcontent-%COMP%]:hover {\n  color: royalblue;\n  transition: color 500ms ease;\n}\n.sidebarMenuInner[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #fff;\n  text-transform: uppercase;\n  font-weight: bold;\n  cursor: pointer;\n  text-decoration: none;\n}\n.linkinconbox[_ngcontent-%COMP%] {\n  position: fixed;\n  bottom: 20px;\n  display: flex;\n  margin: 20px;\n}\n.linkicon[_ngcontent-%COMP%] {\n  -webkit-filter: invert(88%) sepia(61%) saturate(0%) hue-rotate(229deg) brightness(107%) contrast(101%);\n          filter: invert(88%) sepia(61%) saturate(0%) hue-rotate(229deg) brightness(107%) contrast(101%);\n  display: inline-block;\n  border-bottom: solid 2px transparent;\n}\n.linkicon[_ngcontent-%COMP%]:hover {\n  border-bottom: solid 2px royalblue;\n}\n.linkimg[_ngcontent-%COMP%] {\n  width: 50px;\n  padding: 6px;\n}\na[_ngcontent-%COMP%] {\n  color: white;\n  text-decoration: none;\n}\n.contenair[_ngcontent-%COMP%] {\n  margin: -8px;\n}\n.sec-top[_ngcontent-%COMP%] {\n  height: 540px;\n  min-width: 720px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  text-align: center;\n  padding: 20px;\n  color: #141414;\n  background-image: url('imagemosaique.jpg');\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n.sec-main[_ngcontent-%COMP%] {\n  padding: 1px;\n  background-color: #184772;\n  min-height: 500px;\n  min-width: 720px;\n  justify-content: center;\n  align-items: center;\n  text-align: center;\n}\n.whitebar[_ngcontent-%COMP%] {\n  height: 30px;\n  background-color: #FFFFFF;\n}\n.name1[_ngcontent-%COMP%] {\n  font-size: 70px;\n  color: white;\n  line-height: 70px;\n  flex-grow: 0;\n}\n.name2[_ngcontent-%COMP%] {\n  font-size: 36px;\n  color: white;\n  flex-grow: 0;\n}\n.dlcv[_ngcontent-%COMP%] {\n  flex-grow: 0;\n}\n.dlcv[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  padding: 10px;\n  font-size: 24px;\n  border: solid 4px white;\n  border-radius: 5px;\n  color: white;\n}\n.dlcv[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n  padding: 10px;\n  font-size: 24px;\n  border: solid 3px white;\n  border-radius: 5px;\n  color: white;\n  -webkit-filter: invert(50%) sepia(0%) saturate(11%) hue-rotate(143deg) brightness(101%) contrast(93%);\n          filter: invert(50%) sepia(0%) saturate(11%) hue-rotate(143deg) brightness(101%) contrast(93%);\n  transition: all 500ms ease;\n}\n.dlcv[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  height: 40px;\n  margin: 0 20px -10px 0;\n  -webkit-filter: invert(88%) sepia(61%) saturate(0%) hue-rotate(229deg) brightness(107%) contrast(101%);\n          filter: invert(88%) sepia(61%) saturate(0%) hue-rotate(229deg) brightness(107%) contrast(101%);\n}\n.dlcv[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]   [_ngcontent-%COMP%]:hover {\n  -webkit-filter: invert(50%) sepia(0%) saturate(11%) hue-rotate(143deg) brightness(101%) contrast(93%);\n          filter: invert(50%) sepia(0%) saturate(11%) hue-rotate(143deg) brightness(101%) contrast(93%);\n  transition: all 500ms ease;\n}\n.name3[_ngcontent-%COMP%] {\n  font-size: 26px;\n  color: white;\n  z-index: 82;\n  flex-grow: 0;\n}\n.nameempty1[_ngcontent-%COMP%] {\n  flex-grow: 3;\n}\n.namebox[_ngcontent-%COMP%] {\n  flex-grow: 0;\n}\n.nameempty2[_ngcontent-%COMP%] {\n  flex-grow: 1;\n}\n.nameempty3[_ngcontent-%COMP%] {\n  flex-grow: 1;\n}\n.footer[_ngcontent-%COMP%] {\n  color: #FFFFFF;\n  font-size: 8px;\n  height: 300px;\n  min-width: 720px;\n  padding-top: 20px;\n  background: linear-gradient(180deg, #141414 0%, #282828 100%);\n  text-align: center;\n}\n.footer[_ngcontent-%COMP%]   address[_ngcontent-%COMP%] {\n  margin-top: 80px;\n}\n.footer[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  padding: 20px;\n  font-size: 18px;\n}\n.footer[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n  display: inline;\n}\n.footer[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   [_ngcontent-%COMP%]:hover {\n  font-size: 18px;\n  display: inline;\n  background-color: royalblue;\n  box-shadow: 0 5px 10px royalblue;\n  transition: all 500ms ease;\n}\n.btn-square-slant[_ngcontent-%COMP%] {\n  display: inline-block;\n  position: relative;\n  padding: 0.5em 1.4em;\n  text-decoration: none;\n  background: #668ad8;\n  \n  color: #FFF;\n  border-bottom: solid 5px #36528c;\n  \n  border-right: solid 5px #5375bd;\n  \n}\n.btn-square-slant[_ngcontent-%COMP%]:before {\n  content: \" \";\n  position: absolute;\n  bottom: -5px;\n  left: -1px;\n  width: 0;\n  height: 0;\n  border-width: 0 6px 6px 0;\n  border-style: solid;\n  border-color: transparent;\n  border-bottom-color: #FFF;\n}\n.btn-square-slant[_ngcontent-%COMP%]:after {\n  content: \" \";\n  position: absolute;\n  top: -1px;\n  right: -5px;\n  width: 0;\n  height: 0;\n  border-width: 0 6px 6px 0;\n  border-style: solid;\n  border-color: #FFF;\n  border-bottom-color: transparent;\n}\n.btn-square-slant[_ngcontent-%COMP%]:active {\n  \n  border: none;\n  transform: translate(6px, 6px);\n}\n.btn-square-slant[_ngcontent-%COMP%]:active:after, .btn-square-slant[_ngcontent-%COMP%]:active:before {\n  content: none;\n  \n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9DOlxcVXNlcnNcXGh1XFxXZWJzdG9ybVByb2plY3RzXFxzaXRlcG9ydGZvbGlvXFxzaXRlcG9ydGZvbGlvWVQvc3JjXFxhcHBcXGFwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBZ0I7QUNBaEI7Ozs7Ozs7Ozs7Ozs7RUFhRSxTQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDQSx3QkFBQTtFQUNBLG9DQUFBO0FERUY7QUNDQTs7RUFFRSxjQUFBO0FERUY7QUNDQTtFQUNFLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0FERUY7QUNDQTtFQUNFLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDQSx1Q0FBQTtBREVGO0FDQUE7RUFDRSxTQUFBO0FER0Y7QUNBQTtFQUNFLG9CQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0FER0Y7QUNHQTtFQUNFLG9CQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7QURBRjtBQ0dBO0VBQ0Usb0JBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QURBRjtBQ0dBO0VBQ0Usa0JBQUE7RUFDQSxvQkFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtBREFGO0FDR0E7RUFDRSxvQkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBREFGO0FDR0E7RUFDRSx3QkFBQTtBREFGO0FDR0E7RUFDRSxjQUFBO0FEQUY7QUNHQTtFQUNFLG9CQUFBO0VBQ0Esc0JBQUE7RUFDQSxhQUFBO0FEQUY7QUNHQTtFQUNFLG9CQUFBO0VBQ0Esc0JBQUE7RUFDQSxVQUFBO0FEQUY7QUNHQTtFQUNFLG9CQUFBO0VBQ0Esc0JBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0FEQUY7QUNHQTtFQUNFLG9CQUFBO0VBQ0Esc0JBQUE7RUFDQSwwQkFBQTtFQUNBLGlCQUFBO0FEQUY7QUNJQTtFQUNFLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLE9BQUE7RUFDQSxTQUFBO0VBQ0EsNkJBQUE7RUFDQSx1Q0FBQTtFQUNBLDhCQUFBO0VBQ0EsWUFBQTtBRERGO0FDSUE7RUFDRSxrQkFBQTtFQUNBLDhDQUFBO0FEREY7QUNJQTtFQUNFLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLHlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsZUFBQTtFQUNBLGlEQUFBO0FEREY7QUNJQTtFQUNFLGdCQUFBO0VBQ0EsNEJBQUE7QURERjtBQ0lBO0VBQ0UsV0FBQTtFQUNBLHlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EscUJBQUE7QURERjtBQ0lBO0VBQ0UsZUFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBRERGO0FDSUE7RUFDRSxzR0FBQTtVQUFBLDhGQUFBO0VBQ0EscUJBQUE7RUFDQSxvQ0FBQTtBRERGO0FDSUE7RUFDRSxrQ0FBQTtBRERGO0FDSUE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBRERGO0FDS0E7RUFDRSxZQUFBO0VBQ0EscUJBQUE7QURGRjtBQ0tBO0VBQ0UsWUFBQTtBREZGO0FDS0E7RUFDRSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtFQUNBLDBDQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBREZGO0FDS0E7RUFDRSxZQUFBO0VBQ0EseUJBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FERkY7QUNLQTtFQUNFLFlBQUE7RUFDQSx5QkFBQTtBREZGO0FDSUE7RUFDRSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtBRERGO0FDSUE7RUFDRSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7QURERjtBQ0lBO0VBQ0UsWUFBQTtBRERGO0FDSUE7RUFDRSxhQUFBO0VBQ0EsZUFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FEREY7QUNJQTtFQUNFLGFBQUE7RUFDQSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxxR0FBQTtVQUFBLDZGQUFBO0VBQ0EsMEJBQUE7QURERjtBQ0lBO0VBQ0UsWUFBQTtFQUNBLHNCQUFBO0VBQ0Esc0dBQUE7VUFBQSw4RkFBQTtBRERGO0FDSUE7RUFDRSxxR0FBQTtVQUFBLDZGQUFBO0VBQ0EsMEJBQUE7QURERjtBQ0lBO0VBQ0UsZUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBRERGO0FDS0E7RUFDRSxZQUFBO0FERkY7QUNLQTtFQUNFLFlBQUE7QURGRjtBQ0tBO0VBQ0UsWUFBQTtBREZGO0FDS0E7RUFDRSxZQUFBO0FERkY7QUNNQTtFQUNFLGNBQUE7RUFDQSxjQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSw2REFBQTtFQUNBLGtCQUFBO0FESEY7QUNNQTtFQUNFLGdCQUFBO0FESEY7QUNTQTtFQUNFLGFBQUE7RUFDQSxlQUFBO0FETkY7QUNTQTtFQUNFLGVBQUE7QURORjtBQ1NBO0VBQ0UsZUFBQTtFQUNBLGVBQUE7RUFDQSwyQkFBQTtFQUNBLGdDQUFBO0VBQ0EsMEJBQUE7QURORjtBQ1VBO0VBQ0UscUJBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUFxQixPQUFBO0VBQ3JCLFdBQUE7RUFDQSxnQ0FBQTtFQUFrQyxZQUFBO0VBQ2xDLCtCQUFBO0VBQWlDLFlBQUE7QURKbkM7QUNPQTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSx5QkFBQTtBREpGO0FDT0E7RUFDRSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0NBQUE7QURKRjtBQ09BO0VBQ0UsWUFBQTtFQUNBLFlBQUE7RUFFQSw4QkFBQTtBREpGO0FDT0E7RUFDRSxhQUFBO0VBQWUsZUFBQTtBREhqQiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjaGFyc2V0IFwiVVRGLThcIjtcbmh0bWwsIGJvZHksIGRpdiwgc3BhbiwgYXBwbGV0LCBvYmplY3QsIGlmcmFtZSxcbmgxLCBoMiwgaDMsIGg0LCBoNSwgaDYsIHAsIGJsb2NrcXVvdGUsIHByZSxcbmEsIGFiYnIsIGFjcm9ueW0sIGFkZHJlc3MsIGJpZywgY2l0ZSwgY29kZSxcbmRlbCwgZGZuLCBlbSwgaW1nLCBpbnMsIGtiZCwgcSwgcywgc2FtcCxcbnNtYWxsLCBzdHJpa2UsIHN0cm9uZywgc3ViLCBzdXAsIHR0LCB2YXIsXG5iLCB1LCBpLCBjZW50ZXIsXG5kbCwgZHQsIGRkLCBvbCwgdWwsIGxpLFxuZmllbGRzZXQsIGZvcm0sIGxhYmVsLCBsZWdlbmQsXG50YWJsZSwgY2FwdGlvbiwgdGJvZHksIHRmb290LCB0aGVhZCwgdHIsIHRoLCB0ZCxcbmFydGljbGUsIGFzaWRlLCBjYW52YXMsIGRldGFpbHMsIGVtYmVkLFxuZmlndXJlLCBmaWdjYXB0aW9uLCBmb290ZXIsIGhlYWRlciwgaGdyb3VwLFxubWVudSwgbmF2LCBvdXRwdXQsIHJ1YnksIHNlY3Rpb24sIHN1bW1hcnksXG50aW1lLCBtYXJrLCBhdWRpbywgdmlkZW8ge1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmc6IDA7XG4gIGJvcmRlcjogMDtcbiAgdmVydGljYWwtYWxpZ246IGJhc2VsaW5lO1xuICBmb250LWZhbWlseTogXCJXb3JrIFNhbnNcIiwgc2Fucy1zZXJpZjtcbn1cblxuYXJ0aWNsZSwgYXNpZGUsIGRldGFpbHMsIGZpZ2NhcHRpb24sIGZpZ3VyZSxcbmZvb3RlciwgaGVhZGVyLCBoZ3JvdXAsIG1lbnUsIG5hdiwgc2VjdGlvbiB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuXG5odG1sLCBib2R5IHtcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmc6IDA7XG4gIGJvcmRlcjogMDtcbn1cblxuYm9keSB7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbjogMDtcbiAgZm9udC1mYW1pbHk6IFwiVmFyZWxhIFJvdW5kXCIsIHNhbnMtc2VyaWY7XG59XG5cbmgyIHtcbiAgbWFyZ2luOiAwO1xufVxuXG4uc2lkZWJhckljb25Ub2dnbGUge1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zcztcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHdpZHRoOiA0NHB4O1xuICBoZWlnaHQ6IDQ0cHg7XG4gIHotaW5kZXg6IDk2MTtcbiAgdG9wOiAyMnB4O1xuICBsZWZ0OiAyMHB4O1xufVxuXG4uc3Bpbm5lciB7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogNnB4O1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbn1cblxuLmhvcml6b250YWwge1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zcztcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBmbG9hdDogbGVmdDtcbiAgbWFyZ2luLXRvcDogNnB4O1xufVxuXG4uZGlhZ29uYWwucGFydC0xIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zcztcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgZmxvYXQ6IGxlZnQ7XG59XG5cbi5kaWFnb25hbC5wYXJ0LTIge1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zcztcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBmbG9hdDogbGVmdDtcbiAgbWFyZ2luLXRvcDogNnB4O1xufVxuXG5pbnB1dFt0eXBlPWNoZWNrYm94XTpjaGVja2VkIH4gI3NpZGViYXJNZW51IHtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDApO1xufVxuXG5pbnB1dFt0eXBlPWNoZWNrYm94XTpjaGVja2VkIH4gLmNsb3NlYmFyIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbmlucHV0W3R5cGU9Y2hlY2tib3hdIHtcbiAgdHJhbnNpdGlvbjogYWxsIDAuM3M7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbmlucHV0W3R5cGU9Y2hlY2tib3hdOmNoZWNrZWQgfiAuc2lkZWJhckljb25Ub2dnbGUgPiAuaG9yaXpvbnRhbCB7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBvcGFjaXR5OiAwO1xufVxuXG5pbnB1dFt0eXBlPWNoZWNrYm94XTpjaGVja2VkIH4gLnNpZGViYXJJY29uVG9nZ2xlID4gLmRpYWdvbmFsLnBhcnQtMSB7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgxMzVkZWcpO1xuICBtYXJnaW4tdG9wOiAxNnB4O1xufVxuXG5pbnB1dFt0eXBlPWNoZWNrYm94XTpjaGVja2VkIH4gLnNpZGViYXJJY29uVG9nZ2xlID4gLmRpYWdvbmFsLnBhcnQtMiB7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgtMTM1ZGVnKTtcbiAgbWFyZ2luLXRvcDogLTE4cHg7XG59XG5cbiNzaWRlYmFyTWVudSB7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDQ4MHB4O1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIGxlZnQ6IDA7XG4gIG1hcmdpbjogMDtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC00ODBweCk7XG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSAyNTBtcyBlYXNlLWluLW91dDtcbiAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjgpO1xuICB6LWluZGV4OiA5NTA7XG59XG5cbi5zaWRlYmFyTWVudUlubmVyIHtcbiAgbWFyZ2luOiA3MHB4IDAgMCAwO1xuICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjEpO1xufVxuXG4uc2lkZWJhck1lbnVJbm5lciBsaSB7XG4gIGxpc3Qtc3R5bGU6IG5vbmU7XG4gIGNvbG9yOiAjZmZmO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgcGFkZGluZzogMjBweDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjEpO1xufVxuXG4uc2lkZWJhck1lbnVJbm5lciA6aG92ZXIge1xuICBjb2xvcjogcm95YWxibHVlO1xuICB0cmFuc2l0aW9uOiBjb2xvciA1MDBtcyBlYXNlO1xufVxuXG4uc2lkZWJhck1lbnVJbm5lciBsaSBhIHtcbiAgY29sb3I6ICNmZmY7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cblxuLmxpbmtpbmNvbmJveCB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgYm90dG9tOiAyMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBtYXJnaW46IDIwcHg7XG59XG5cbi5saW5raWNvbiB7XG4gIGZpbHRlcjogaW52ZXJ0KDg4JSkgc2VwaWEoNjElKSBzYXR1cmF0ZSgwJSkgaHVlLXJvdGF0ZSgyMjlkZWcpIGJyaWdodG5lc3MoMTA3JSkgY29udHJhc3QoMTAxJSk7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgYm9yZGVyLWJvdHRvbTogc29saWQgMnB4IHRyYW5zcGFyZW50O1xufVxuXG4ubGlua2ljb246aG92ZXIge1xuICBib3JkZXItYm90dG9tOiBzb2xpZCAycHggcm95YWxibHVlO1xufVxuXG4ubGlua2ltZyB7XG4gIHdpZHRoOiA1MHB4O1xuICBwYWRkaW5nOiA2cHg7XG59XG5cbmEge1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cblxuLmNvbnRlbmFpciB7XG4gIG1hcmdpbjogLThweDtcbn1cblxuLnNlYy10b3Age1xuICBoZWlnaHQ6IDU0MHB4O1xuICBtaW4td2lkdGg6IDcyMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nOiAyMHB4O1xuICBjb2xvcjogIzE0MTQxNDtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vYXNzZXRzL2ltYWdlbW9zYWlxdWUuanBnXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xufVxuXG4uc2VjLW1haW4ge1xuICBwYWRkaW5nOiAxcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMxODQ3NzI7XG4gIG1pbi1oZWlnaHQ6IDUwMHB4O1xuICBtaW4td2lkdGg6IDcyMHB4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ud2hpdGViYXIge1xuICBoZWlnaHQ6IDMwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNGRkZGRkY7XG59XG5cbi5uYW1lMSB7XG4gIGZvbnQtc2l6ZTogNzBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBsaW5lLWhlaWdodDogNzBweDtcbiAgZmxleC1ncm93OiAwO1xufVxuXG4ubmFtZTIge1xuICBmb250LXNpemU6IDM2cHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZmxleC1ncm93OiAwO1xufVxuXG4uZGxjdiB7XG4gIGZsZXgtZ3JvdzogMDtcbn1cblxuLmRsY3YgYSB7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMjRweDtcbiAgYm9yZGVyOiBzb2xpZCA0cHggd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uZGxjdiBhOmhvdmVyIHtcbiAgcGFkZGluZzogMTBweDtcbiAgZm9udC1zaXplOiAyNHB4O1xuICBib3JkZXI6IHNvbGlkIDNweCB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBjb2xvcjogd2hpdGU7XG4gIGZpbHRlcjogaW52ZXJ0KDUwJSkgc2VwaWEoMCUpIHNhdHVyYXRlKDExJSkgaHVlLXJvdGF0ZSgxNDNkZWcpIGJyaWdodG5lc3MoMTAxJSkgY29udHJhc3QoOTMlKTtcbiAgdHJhbnNpdGlvbjogYWxsIDUwMG1zIGVhc2U7XG59XG5cbi5kbGN2IGltZyB7XG4gIGhlaWdodDogNDBweDtcbiAgbWFyZ2luOiAwIDIwcHggLTEwcHggMDtcbiAgZmlsdGVyOiBpbnZlcnQoODglKSBzZXBpYSg2MSUpIHNhdHVyYXRlKDAlKSBodWUtcm90YXRlKDIyOWRlZykgYnJpZ2h0bmVzcygxMDclKSBjb250cmFzdCgxMDElKTtcbn1cblxuLmRsY3YgaW1nIDpob3ZlciB7XG4gIGZpbHRlcjogaW52ZXJ0KDUwJSkgc2VwaWEoMCUpIHNhdHVyYXRlKDExJSkgaHVlLXJvdGF0ZSgxNDNkZWcpIGJyaWdodG5lc3MoMTAxJSkgY29udHJhc3QoOTMlKTtcbiAgdHJhbnNpdGlvbjogYWxsIDUwMG1zIGVhc2U7XG59XG5cbi5uYW1lMyB7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgY29sb3I6IHdoaXRlO1xuICB6LWluZGV4OiA4MjtcbiAgZmxleC1ncm93OiAwO1xufVxuXG4ubmFtZWVtcHR5MSB7XG4gIGZsZXgtZ3JvdzogMztcbn1cblxuLm5hbWVib3gge1xuICBmbGV4LWdyb3c6IDA7XG59XG5cbi5uYW1lZW1wdHkyIHtcbiAgZmxleC1ncm93OiAxO1xufVxuXG4ubmFtZWVtcHR5MyB7XG4gIGZsZXgtZ3JvdzogMTtcbn1cblxuLmZvb3RlciB7XG4gIGNvbG9yOiAjRkZGRkZGO1xuICBmb250LXNpemU6IDhweDtcbiAgaGVpZ2h0OiAzMDBweDtcbiAgbWluLXdpZHRoOiA3MjBweDtcbiAgcGFkZGluZy10b3A6IDIwcHg7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgxODBkZWcsICMxNDE0MTQgMCUsICMyODI4MjggMTAwJSk7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmZvb3RlciBhZGRyZXNzIHtcbiAgbWFyZ2luLXRvcDogODBweDtcbn1cblxuLmZvb3RlciBhIHtcbiAgcGFkZGluZzogMjBweDtcbiAgZm9udC1zaXplOiAxOHB4O1xufVxuXG4uZm9vdGVyIGxpIHtcbiAgZGlzcGxheTogaW5saW5lO1xufVxuXG4uZm9vdGVyIGxpIDpob3ZlciB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZGlzcGxheTogaW5saW5lO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByb3lhbGJsdWU7XG4gIGJveC1zaGFkb3c6IDAgNXB4IDEwcHggcm95YWxibHVlO1xuICB0cmFuc2l0aW9uOiBhbGwgNTAwbXMgZWFzZTtcbn1cblxuLmJ0bi1zcXVhcmUtc2xhbnQge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgcGFkZGluZzogMC41ZW0gMS40ZW07XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgYmFja2dyb3VuZDogIzY2OGFkODtcbiAgLyrjg5zjgr/jg7PoibIqL1xuICBjb2xvcjogI0ZGRjtcbiAgYm9yZGVyLWJvdHRvbTogc29saWQgNXB4ICMzNjUyOGM7XG4gIC8q44Oc44K/44Oz6Imy44KI44KK5pqX44KB44GrKi9cbiAgYm9yZGVyLXJpZ2h0OiBzb2xpZCA1cHggIzUzNzViZDtcbiAgLyrjg5zjgr/jg7PoibLjgojjgormmpfjgoHjgasqL1xufVxuXG4uYnRuLXNxdWFyZS1zbGFudDpiZWZvcmUge1xuICBjb250ZW50OiBcIiBcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IC01cHg7XG4gIGxlZnQ6IC0xcHg7XG4gIHdpZHRoOiAwO1xuICBoZWlnaHQ6IDA7XG4gIGJvcmRlci13aWR0aDogMCA2cHggNnB4IDA7XG4gIGJvcmRlci1zdHlsZTogc29saWQ7XG4gIGJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGJvcmRlci1ib3R0b20tY29sb3I6ICNGRkY7XG59XG5cbi5idG4tc3F1YXJlLXNsYW50OmFmdGVyIHtcbiAgY29udGVudDogXCIgXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAtMXB4O1xuICByaWdodDogLTVweDtcbiAgd2lkdGg6IDA7XG4gIGhlaWdodDogMDtcbiAgYm9yZGVyLXdpZHRoOiAwIDZweCA2cHggMDtcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgYm9yZGVyLWNvbG9yOiAjRkZGO1xuICBib3JkZXItYm90dG9tLWNvbG9yOiB0cmFuc3BhcmVudDtcbn1cblxuLmJ0bi1zcXVhcmUtc2xhbnQ6YWN0aXZlIHtcbiAgLyrjg5zjgr/jg7PjgpLmirzjgZfjgZ/jgajjgY0qL1xuICBib3JkZXI6IG5vbmU7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoNnB4LCA2cHgpO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSg2cHgsIDZweCk7XG59XG5cbi5idG4tc3F1YXJlLXNsYW50OmFjdGl2ZTphZnRlciwgLmJ0bi1zcXVhcmUtc2xhbnQ6YWN0aXZlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IG5vbmU7XG4gIC8q44Oc44K/44Oz44KS5oq844GZ44Go57ea44GM5raI44GI44KLKi9cbn0iLCJodG1sLCBib2R5LCBkaXYsIHNwYW4sIGFwcGxldCwgb2JqZWN0LCBpZnJhbWUsXHJcbmgxLCBoMiwgaDMsIGg0LCBoNSwgaDYsIHAsIGJsb2NrcXVvdGUsIHByZSxcclxuYSwgYWJiciwgYWNyb255bSwgYWRkcmVzcywgYmlnLCBjaXRlLCBjb2RlLFxyXG5kZWwsIGRmbiwgZW0sIGltZywgaW5zLCBrYmQsIHEsIHMsIHNhbXAsXHJcbnNtYWxsLCBzdHJpa2UsIHN0cm9uZywgc3ViLCBzdXAsIHR0LCB2YXIsXHJcbmIsIHUsIGksIGNlbnRlcixcclxuZGwsIGR0LCBkZCwgb2wsIHVsLCBsaSxcclxuZmllbGRzZXQsIGZvcm0sIGxhYmVsLCBsZWdlbmQsXHJcbnRhYmxlLCBjYXB0aW9uLCB0Ym9keSwgdGZvb3QsIHRoZWFkLCB0ciwgdGgsIHRkLFxyXG5hcnRpY2xlLCBhc2lkZSwgY2FudmFzLCBkZXRhaWxzLCBlbWJlZCxcclxuZmlndXJlLCBmaWdjYXB0aW9uLCBmb290ZXIsIGhlYWRlciwgaGdyb3VwLFxyXG5tZW51LCBuYXYsIG91dHB1dCwgcnVieSwgc2VjdGlvbiwgc3VtbWFyeSxcclxudGltZSwgbWFyaywgYXVkaW8sIHZpZGVvIHtcclxuICBtYXJnaW46IDA7XHJcbiAgcGFkZGluZzogMDtcclxuICBib3JkZXI6IDA7XHJcbiAgdmVydGljYWwtYWxpZ246IGJhc2VsaW5lO1xyXG4gIGZvbnQtZmFtaWx5OiAnV29yayBTYW5zJywgc2Fucy1zZXJpZjtcclxufVxyXG5cclxuYXJ0aWNsZSwgYXNpZGUsIGRldGFpbHMsIGZpZ2NhcHRpb24sIGZpZ3VyZSxcclxuZm9vdGVyLCBoZWFkZXIsIGhncm91cCwgbWVudSwgbmF2LCBzZWN0aW9uIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxufVxyXG5cclxuaHRtbCwgYm9keSB7XHJcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xyXG4gIG1hcmdpbjogMDtcclxuICBwYWRkaW5nOiAwO1xyXG4gIGJvcmRlcjogMDtcclxufVxyXG5cclxuYm9keSB7XHJcbiAgYmFja2dyb3VuZDogI2ZmZjtcclxuICBwYWRkaW5nOiAwO1xyXG4gIG1hcmdpbjogMDtcclxuICBmb250LWZhbWlseTogJ1ZhcmVsYSBSb3VuZCcsIHNhbnMtc2VyaWY7XHJcbn1cclxuaDIge1xyXG4gIG1hcmdpbjogMDtcclxufVxyXG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xyXG4uc2lkZWJhckljb25Ub2dnbGUge1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICB3aWR0aDogNDRweDtcclxuICBoZWlnaHQ6IDQ0cHg7XHJcbiAgei1pbmRleDogOTYxO1xyXG4gIHRvcDogMjJweDtcclxuICBsZWZ0OiAyMHB4O1xyXG59XHJcblxyXG4uY2xvc2ViYXIge1xyXG59XHJcblxyXG4uc3Bpbm5lciB7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuM3M7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgaGVpZ2h0OiA2cHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxufVxyXG5cclxuLmhvcml6b250YWwge1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIG1hcmdpbi10b3A6IDZweDtcclxufVxyXG5cclxuLmRpYWdvbmFsLnBhcnQtMSB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbn1cclxuXHJcbi5kaWFnb25hbC5wYXJ0LTIge1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIG1hcmdpbi10b3A6IDZweDtcclxufVxyXG5cclxuaW5wdXRbdHlwZT1cImNoZWNrYm94XCJdOmNoZWNrZWQgfiAjc2lkZWJhck1lbnUge1xyXG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgwKTtcclxufVxyXG5cclxuaW5wdXRbdHlwZT1cImNoZWNrYm94XCJdOmNoZWNrZWQgfiAuY2xvc2ViYXIge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPVwiY2hlY2tib3hcIl0ge1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxuaW5wdXRbdHlwZT1cImNoZWNrYm94XCJdOmNoZWNrZWQgfiAuc2lkZWJhckljb25Ub2dnbGUgPiAuaG9yaXpvbnRhbCB7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuM3M7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICBvcGFjaXR5OiAwO1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPVwiY2hlY2tib3hcIl06Y2hlY2tlZCB+IC5zaWRlYmFySWNvblRvZ2dsZSA+IC5kaWFnb25hbC5wYXJ0LTEge1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgdHJhbnNmb3JtOiByb3RhdGUoMTM1ZGVnKTtcclxuICBtYXJnaW4tdG9wOiAxNnB4O1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPVwiY2hlY2tib3hcIl06Y2hlY2tlZCB+IC5zaWRlYmFySWNvblRvZ2dsZSA+IC5kaWFnb25hbC5wYXJ0LTIge1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgdHJhbnNmb3JtOiByb3RhdGUoLTEzNWRlZyk7XHJcbiAgbWFyZ2luLXRvcDogLTE4cHg7XHJcbn1cclxuXHJcbi8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXHJcbiNzaWRlYmFyTWVudSB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIHdpZHRoOiA0ODBweDtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgbGVmdDogMDtcclxuICBtYXJnaW46IDA7XHJcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC00ODBweCk7XHJcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDI1MG1zIGVhc2UtaW4tb3V0O1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC44MCk7XHJcbiAgei1pbmRleDogOTUwO1xyXG59XHJcblxyXG4uc2lkZWJhck1lbnVJbm5lciB7XHJcbiAgbWFyZ2luOiA3MHB4IDAgMCAwO1xyXG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMTApO1xyXG59XHJcblxyXG4uc2lkZWJhck1lbnVJbm5lciBsaSB7XHJcbiAgbGlzdC1zdHlsZTogbm9uZTtcclxuICBjb2xvcjogI2ZmZjtcclxuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIHBhZGRpbmc6IDIwcHg7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMTApO1xyXG59XHJcblxyXG4uc2lkZWJhck1lbnVJbm5lciA6aG92ZXIge1xyXG4gIGNvbG9yOiByb3lhbGJsdWU7XHJcbiAgdHJhbnNpdGlvbjogY29sb3IgNTAwbXMgZWFzZTtcclxufVxyXG5cclxuLnNpZGViYXJNZW51SW5uZXIgbGkgYSB7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG59XHJcblxyXG4ubGlua2luY29uYm94IHtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgYm90dG9tOiAyMHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgbWFyZ2luOiAyMHB4O1xyXG59XHJcblxyXG4ubGlua2ljb24ge1xyXG4gIGZpbHRlcjogaW52ZXJ0KDg4JSkgc2VwaWEoNjElKSBzYXR1cmF0ZSgwJSkgaHVlLXJvdGF0ZSgyMjlkZWcpIGJyaWdodG5lc3MoMTA3JSkgY29udHJhc3QoMTAxJSk7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIGJvcmRlci1ib3R0b206IHNvbGlkIDJweCB0cmFuc3BhcmVudDtcclxufVxyXG5cclxuLmxpbmtpY29uOmhvdmVyIHtcclxuICBib3JkZXItYm90dG9tOiBzb2xpZCAycHggcm95YWxibHVlO1xyXG59XHJcblxyXG4ubGlua2ltZyB7XHJcbiAgd2lkdGg6IDUwcHg7XHJcbiAgcGFkZGluZzogNnB4O1xyXG59XHJcblxyXG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cclxuYSB7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxufVxyXG5cclxuLmNvbnRlbmFpciB7XHJcbiAgbWFyZ2luOiAtOHB4O1xyXG59XHJcblxyXG4uc2VjLXRvcCB7XHJcbiAgaGVpZ2h0OiA1NDBweDtcclxuICBtaW4td2lkdGg6IDcyMHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBwYWRkaW5nOiAyMHB4O1xyXG4gIGNvbG9yOiByZ2IoMjAsIDIwLCAyMCk7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vYXNzZXRzL2ltYWdlbW9zYWlxdWUuanBnXCIpO1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxufVxyXG5cclxuLnNlYy1tYWluIHtcclxuICBwYWRkaW5nOiAxcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzE4NDc3MjtcclxuICBtaW4taGVpZ2h0OiA1MDBweDtcclxuICBtaW4td2lkdGg6IDcyMHB4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4ud2hpdGViYXJ7XHJcbiAgaGVpZ2h0OiAzMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNGRkZGRkY7XHJcbn1cclxuLm5hbWUxIHtcclxuICBmb250LXNpemU6IDcwcHg7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGxpbmUtaGVpZ2h0OiA3MHB4O1xyXG4gIGZsZXgtZ3JvdzogMDtcclxufVxyXG5cclxuLm5hbWUyIHtcclxuICBmb250LXNpemU6IDM2cHg7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGZsZXgtZ3JvdzogMDtcclxufVxyXG5cclxuLmRsY3Yge1xyXG4gIGZsZXgtZ3JvdzogMDtcclxufVxyXG5cclxuLmRsY3YgYSB7XHJcbiAgcGFkZGluZzogMTBweDtcclxuICBmb250LXNpemU6IDI0cHg7XHJcbiAgYm9yZGVyOiBzb2xpZCA0cHggd2hpdGU7XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmRsY3YgYTpob3ZlciB7XHJcbiAgcGFkZGluZzogMTBweDtcclxuICBmb250LXNpemU6IDI0cHg7XHJcbiAgYm9yZGVyOiBzb2xpZCAzcHggd2hpdGU7XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBmaWx0ZXI6IGludmVydCg1MCUpIHNlcGlhKDAlKSBzYXR1cmF0ZSgxMSUpIGh1ZS1yb3RhdGUoMTQzZGVnKSBicmlnaHRuZXNzKDEwMSUpIGNvbnRyYXN0KDkzJSk7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDUwMG1zIGVhc2U7XHJcbn1cclxuXHJcbi5kbGN2IGltZyB7XHJcbiAgaGVpZ2h0OiA0MHB4O1xyXG4gIG1hcmdpbjogMCAyMHB4IC0xMHB4IDA7XHJcbiAgZmlsdGVyOiBpbnZlcnQoODglKSBzZXBpYSg2MSUpIHNhdHVyYXRlKDAlKSBodWUtcm90YXRlKDIyOWRlZykgYnJpZ2h0bmVzcygxMDclKSBjb250cmFzdCgxMDElKTtcclxufVxyXG5cclxuLmRsY3YgaW1nIDpob3ZlciB7XHJcbiAgZmlsdGVyOiBpbnZlcnQoNTAlKSBzZXBpYSgwJSkgc2F0dXJhdGUoMTElKSBodWUtcm90YXRlKDE0M2RlZykgYnJpZ2h0bmVzcygxMDElKSBjb250cmFzdCg5MyUpO1xyXG4gIHRyYW5zaXRpb246IGFsbCA1MDBtcyBlYXNlO1xyXG59XHJcblxyXG4ubmFtZTMge1xyXG4gIGZvbnQtc2l6ZTogMjZweDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgei1pbmRleDogODI7XHJcbiAgZmxleC1ncm93OiAwO1xyXG59XHJcblxyXG5cclxuLm5hbWVlbXB0eTEge1xyXG4gIGZsZXgtZ3JvdzogMztcclxufVxyXG5cclxuLm5hbWVib3gge1xyXG4gIGZsZXgtZ3JvdzogMDtcclxufVxyXG5cclxuLm5hbWVlbXB0eTIge1xyXG4gIGZsZXgtZ3JvdzogMTtcclxufVxyXG5cclxuLm5hbWVlbXB0eTMge1xyXG4gIGZsZXgtZ3JvdzogMTtcclxufVxyXG5cclxuLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXHJcbi5mb290ZXIge1xyXG4gIGNvbG9yOiAjRkZGRkZGO1xyXG4gIGZvbnQtc2l6ZTogOHB4O1xyXG4gIGhlaWdodDogMzAwcHg7XHJcbiAgbWluLXdpZHRoOiA3MjBweDtcclxuICBwYWRkaW5nLXRvcDogMjBweDtcclxuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMTgwZGVnLCByZ2IoMjAsIDIwLCAyMCkgMCUsIHJnYig0MCwgNDAsIDQwKSAxMDAlKTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5mb290ZXIgYWRkcmVzcyB7XHJcbiAgbWFyZ2luLXRvcDogODBweDtcclxufVxyXG5cclxuLmZvb3RlciB1bCB7XHJcbn1cclxuXHJcbi5mb290ZXIgYSB7XHJcbiAgcGFkZGluZzogMjBweDtcclxuICBmb250LXNpemU6IDE4cHg7XHJcbn1cclxuXHJcbi5mb290ZXIgbGkge1xyXG4gIGRpc3BsYXk6IGlubGluZTtcclxufVxyXG5cclxuLmZvb3RlciBsaSA6aG92ZXIge1xyXG4gIGZvbnQtc2l6ZTogMThweDtcclxuICBkaXNwbGF5OiBpbmxpbmU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcm95YWxibHVlO1xyXG4gIGJveC1zaGFkb3c6IDAgNXB4IDEwcHggcm95YWxibHVlO1xyXG4gIHRyYW5zaXRpb246IGFsbCA1MDBtcyBlYXNlO1xyXG59XHJcblxyXG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXHJcbi5idG4tc3F1YXJlLXNsYW50IHtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHBhZGRpbmc6IDAuNWVtIDEuNGVtO1xyXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICBiYWNrZ3JvdW5kOiAjNjY4YWQ4OyAvKuODnOOCv+ODs+iJsiovXHJcbiAgY29sb3I6ICNGRkY7XHJcbiAgYm9yZGVyLWJvdHRvbTogc29saWQgNXB4ICMzNjUyOGM7IC8q44Oc44K/44Oz6Imy44KI44KK5pqX44KB44GrKi9cclxuICBib3JkZXItcmlnaHQ6IHNvbGlkIDVweCAjNTM3NWJkOyAvKuODnOOCv+ODs+iJsuOCiOOCiuaal+OCgeOBqyovXHJcbn1cclxuXHJcbi5idG4tc3F1YXJlLXNsYW50OmJlZm9yZSB7XHJcbiAgY29udGVudDogXCIgXCI7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGJvdHRvbTogLTVweDtcclxuICBsZWZ0OiAtMXB4O1xyXG4gIHdpZHRoOiAwO1xyXG4gIGhlaWdodDogMDtcclxuICBib3JkZXItd2lkdGg6IDAgNnB4IDZweCAwO1xyXG4gIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICBib3JkZXItYm90dG9tLWNvbG9yOiAjRkZGO1xyXG59XHJcblxyXG4uYnRuLXNxdWFyZS1zbGFudDphZnRlciB7XHJcbiAgY29udGVudDogXCIgXCI7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogLTFweDtcclxuICByaWdodDogLTVweDtcclxuICB3aWR0aDogMDtcclxuICBoZWlnaHQ6IDA7XHJcbiAgYm9yZGVyLXdpZHRoOiAwIDZweCA2cHggMDtcclxuICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gIGJvcmRlci1jb2xvcjogI0ZGRjtcclxuICBib3JkZXItYm90dG9tLWNvbG9yOiB0cmFuc3BhcmVudDtcclxufVxyXG5cclxuLmJ0bi1zcXVhcmUtc2xhbnQ6YWN0aXZlIHtcclxuICAvKuODnOOCv+ODs+OCkuaKvOOBl+OBn+OBqOOBjSovXHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoNnB4LCA2cHgpO1xyXG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKDZweCwgNnB4KTtcclxufVxyXG5cclxuLmJ0bi1zcXVhcmUtc2xhbnQ6YWN0aXZlOmFmdGVyLCAuYnRuLXNxdWFyZS1zbGFudDphY3RpdmU6YmVmb3JlIHtcclxuICBjb250ZW50OiBub25lOyAvKuODnOOCv+ODs+OCkuaKvOOBmeOBqOe3muOBjOa2iOOBiOOCiyovXHJcbn1cclxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.scss']
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./contact/contact.component */ "./src/app/contact/contact.component.ts");
/* harmony import */ var _competence_competence_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./competence/competence.component */ "./src/app/competence/competence.component.ts");
/* harmony import */ var _portfolio_portfolio_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./portfolio/portfolio.component */ "./src/app/portfolio/portfolio.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
/* harmony import */ var _pagedev_pagedev_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./pagedev/pagedev.component */ "./src/app/pagedev/pagedev.component.ts");
/* harmony import */ var _experience_experience_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./experience/experience.component */ "./src/app/experience/experience.component.ts");
/* harmony import */ var _loisir_loisir_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./loisir/loisir.component */ "./src/app/loisir/loisir.component.ts");


















class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
            _angular_fire__WEBPACK_IMPORTED_MODULE_11__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].firebase),
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_12__["AngularFirestoreModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
        _contact_contact_component__WEBPACK_IMPORTED_MODULE_4__["ContactComponent"],
        _competence_competence_component__WEBPACK_IMPORTED_MODULE_5__["CompetenceComponent"],
        _portfolio_portfolio_component__WEBPACK_IMPORTED_MODULE_6__["PortfolioComponent"],
        _home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"],
        _pagedev_pagedev_component__WEBPACK_IMPORTED_MODULE_13__["PagedevComponent"],
        _experience_experience_component__WEBPACK_IMPORTED_MODULE_14__["ExperienceComponent"],
        _loisir_loisir_component__WEBPACK_IMPORTED_MODULE_15__["LoisirComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"], _angular_fire__WEBPACK_IMPORTED_MODULE_11__["AngularFireModule"], _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_12__["AngularFirestoreModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                    _contact_contact_component__WEBPACK_IMPORTED_MODULE_4__["ContactComponent"],
                    _competence_competence_component__WEBPACK_IMPORTED_MODULE_5__["CompetenceComponent"],
                    _portfolio_portfolio_component__WEBPACK_IMPORTED_MODULE_6__["PortfolioComponent"],
                    _home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"],
                    _pagedev_pagedev_component__WEBPACK_IMPORTED_MODULE_13__["PagedevComponent"],
                    _experience_experience_component__WEBPACK_IMPORTED_MODULE_14__["ExperienceComponent"],
                    _loisir_loisir_component__WEBPACK_IMPORTED_MODULE_15__["LoisirComponent"],
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
                    _angular_fire__WEBPACK_IMPORTED_MODULE_11__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].firebase),
                    _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_12__["AngularFirestoreModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
                    _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"]
                ],
                providers: [],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/competence/competence.component.ts":
/*!****************************************************!*\
  !*** ./src/app/competence/competence.component.ts ***!
  \****************************************************/
/*! exports provided: CompetenceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompetenceComponent", function() { return CompetenceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class CompetenceComponent {
    constructor() {
    }
    ngOnInit() {
    }
}
CompetenceComponent.ɵfac = function CompetenceComponent_Factory(t) { return new (t || CompetenceComponent)(); };
CompetenceComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CompetenceComponent, selectors: [["app-competence"]], decls: 93, vars: 0, consts: [[1, "competencecontainer"], [1, "skil-box"], [1, "icon-js"], [1, "skil-text"], [1, "skilbar"], [1, "skilbar-js"], [1, "icon-react"], [1, "skilbar-r"], [1, "icon-angular"], [1, "skilbar-a"], ["src", "../../assets/icons8-nodejs-100.png", "alt", "", 1, "icon-node"], [1, "skilbar-node"], [1, "skil-other-box"], [1, "skil-others"], [1, "langue"], [1, "c100", "p77", "big", "green"], [1, "slice"], [1, "bar"], [1, "fill"], [1, "c100", "p51", "big", "blue"], [1, "c100", "p100", "big", "red"]], template: function CompetenceComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "COMPETENCES");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Outils Num\u00E9riques");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "span", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "span", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Javascript");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, " - boucle for, array / object / Promise / if / switch / falsy / etc.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "span", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "span", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "React.js");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, " \u2013 Authentification / Cr\u00E9ation d\u2019un component et class / Echange Database / Changer state selon des \u00E9vents / Routing ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "span", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "span", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Angular");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, " - Cr\u00E9ation d\u2019un component / Routing ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Ce siteCV est cr\u00E9e par Angular et FireBase");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "img", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "span", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Node.js");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, " \u2013 CRUD / patch / Cr\u00E9er une function et tester");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "Typescript");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "React-redux");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "HTML5, CSS3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "Boostrap");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "Git");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "GitHub");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57, "GitLab");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "Firebase");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Firestore");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Slack");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, "Trello");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "Adobe illustrator");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "Word, Excel, Powerpoint");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "Comp\u00E9tence humaine : capacit\u00E9 \u00E0 travailler en \u00E9quipe");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "Langues");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, "FRANCAIS");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](79, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](80, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](83, "ANGLAIS");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](85, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](86, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](89, "JAPONAIS");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](91, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](92, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["h2[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 100px;\n  font-size: 40px;\n}\n\nh3[_ngcontent-%COMP%] {\n  position: relative;\n  color: #00caf4;\n  text-align: center;\n  font-size: 26px;\n}\n\n.competencecontainer[_ngcontent-%COMP%] {\n  width: 720px;\n  margin: 0 auto 0;\n}\n\n.langue[_ngcontent-%COMP%] {\n  margin: 0 0 100px 0;\n  width: 720px;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n}\n\n.langue[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%] {\n  flex-grow: 0;\n}\n\n.skil-box[_ngcontent-%COMP%] {\n  margin: 50px auto 0;\n  width: 720px;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n}\n\n.icon-js[_ngcontent-%COMP%] {\n  background-image: url('icons8-javascript-100.png');\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 100px;\n  width: 100px;\n  -webkit-filter: invert(88%) sepia(100%) saturate(2929%) hue-rotate(104deg) brightness(99%) contrast(104%);\n          filter: invert(88%) sepia(100%) saturate(2929%) hue-rotate(104deg) brightness(99%) contrast(104%);\n}\n\n.icon-react[_ngcontent-%COMP%] {\n  background-image: url('icons8-react-native-100.png');\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 100px;\n  width: 100px;\n  -webkit-filter: invert(88%) sepia(100%) saturate(2929%) hue-rotate(104deg) brightness(99%) contrast(104%);\n          filter: invert(88%) sepia(100%) saturate(2929%) hue-rotate(104deg) brightness(99%) contrast(104%);\n}\n\n.icon-angular[_ngcontent-%COMP%] {\n  background-image: url('icons8-angularjs-100.png');\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 100px;\n  width: 100px;\n  -webkit-filter: invert(88%) sepia(100%) saturate(2929%) hue-rotate(104deg) brightness(99%) contrast(104%);\n          filter: invert(88%) sepia(100%) saturate(2929%) hue-rotate(104deg) brightness(99%) contrast(104%);\n}\n\n.icon-node[_ngcontent-%COMP%] {\n  background-image: url('icons8-nodejs-100.png');\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 100px;\n  width: 100px;\n  -webkit-filter: invert(88%) sepia(100%) saturate(2929%) hue-rotate(104deg) brightness(99%) contrast(104%);\n          filter: invert(88%) sepia(100%) saturate(2929%) hue-rotate(104deg) brightness(99%) contrast(104%);\n}\n\n.skil-text[_ngcontent-%COMP%] {\n  width: 580px;\n  margin: 30px;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  text-align: center;\n}\n\n.skilbar[_ngcontent-%COMP%] {\n  width: 470px;\n  height: 20px;\n  background-color: silver;\n  border-radius: 10px;\n}\n\n.skilbar-js[_ngcontent-%COMP%] {\n  width: 90%;\n  height: 20px;\n  background-color: orange;\n  border-radius: 10px;\n}\n\n.skilbar-r[_ngcontent-%COMP%] {\n  width: 70%;\n  height: 20px;\n  background-color: yellow;\n  border-radius: 10px;\n}\n\n.skilbar-a[_ngcontent-%COMP%] {\n  width: 50%;\n  height: 20px;\n  background-color: red;\n  border-radius: 10px;\n}\n\n.skilbar-node[_ngcontent-%COMP%] {\n  width: 85%;\n  height: 20px;\n  background-color: mediumspringgreen;\n  border-radius: 10px;\n}\n\n.skil-text[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  font-size: 20px;\n  color: #ffffff;\n}\n\n.skil-other-box[_ngcontent-%COMP%] {\n  margin: 50px auto 0;\n  width: 720px;\n  display: flex;\n  flex-direction: row;\n  flex-wrap: wrap;\n  justify-content: center;\n  padding-bottom: 60px;\n}\n\n.skil-others[_ngcontent-%COMP%] {\n  font-size: 20px;\n  color: white;\n  padding: 10px 20px;\n  margin: 10px 20px;\n  border-radius: 40px;\n  border: solid 2px #00caf4;\n  background-color: transparent;\n}\n\n.rect-auto[_ngcontent-%COMP%], .c100.p51[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p52[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p53[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p54[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p55[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p56[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p57[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p58[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p59[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p60[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p61[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p62[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p63[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p64[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p65[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p66[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p67[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p68[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p69[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p70[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p71[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p72[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p73[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p74[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p75[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p76[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p77[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p78[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p79[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p80[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p81[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p82[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p83[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p84[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p85[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p86[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p87[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p88[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p89[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p90[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p91[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p92[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p93[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p94[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p95[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p96[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p97[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p98[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p99[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%], .c100.p100[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%] {\n  clip: rect(auto, auto, auto, auto);\n}\n\n.pie[_ngcontent-%COMP%], .c100[_ngcontent-%COMP%]   .bar[_ngcontent-%COMP%], .c100.p51[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p52[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p53[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p54[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p55[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p56[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p57[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p58[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p59[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p60[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p61[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p62[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p63[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p64[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p65[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p66[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p67[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p68[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p69[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p70[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p71[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p72[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p73[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p74[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p75[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p76[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p77[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p78[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p79[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p80[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p81[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p82[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p83[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p84[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p85[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p86[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p87[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p88[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p89[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p90[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p91[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p92[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p93[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p94[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p95[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p96[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p97[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p98[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p99[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%], .c100.p100[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%] {\n  position: absolute;\n  border: 0.08em solid #307bbb;\n  width: 0.84em;\n  height: 0.84em;\n  clip: rect(0em, 0.5em, 1em, 0em);\n  border-radius: 50%;\n}\n\n.c100[_ngcontent-%COMP%] {\n  position: relative;\n  font-size: 120px;\n  width: 1em;\n  height: 1em;\n  border-radius: 50%;\n  float: left;\n  margin: 50px 30px;\n  background-color: gray;\n}\n\n.c100[_ngcontent-%COMP%]   *[_ngcontent-%COMP%], .c100[_ngcontent-%COMP%]   *[_ngcontent-%COMP%]:before, .c100[_ngcontent-%COMP%]   *[_ngcontent-%COMP%]:after {\n  box-sizing: content-box;\n}\n\n.c100.center[_ngcontent-%COMP%] {\n  float: none;\n  margin: 0 auto;\n}\n\n.c100.big[_ngcontent-%COMP%] {\n  font-size: 170px;\n}\n\n.c100.small[_ngcontent-%COMP%] {\n  font-size: 80px;\n}\n\n.c100[_ngcontent-%COMP%]    > span[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  z-index: 1;\n  font-size: 0.15em;\n  color: #FFFFFF;\n  display: block;\n  text-align: center;\n  white-space: nowrap;\n}\n\n.c100[_ngcontent-%COMP%]:after {\n  position: absolute;\n  top: 0.08em;\n  left: 0.08em;\n  display: block;\n  content: \" \";\n  border-radius: 50%;\n  background-color: #434da2;\n  width: 0.84em;\n  height: 0.84em;\n}\n\n.c100[_ngcontent-%COMP%]   .slice[_ngcontent-%COMP%] {\n  position: absolute;\n  width: 1em;\n  height: 1em;\n  clip: rect(0em, 1em, 1em, 0.5em);\n}\n\n.c100.p51[_ngcontent-%COMP%]   .bar[_ngcontent-%COMP%] {\n  transform: rotate(183.6deg);\n}\n\n.c100.p77[_ngcontent-%COMP%]   .bar[_ngcontent-%COMP%] {\n  transform: rotate(277.2deg);\n}\n\n.c100.p100[_ngcontent-%COMP%]   .bar[_ngcontent-%COMP%] {\n  transform: rotate(360deg);\n}\n\n.c100.green[_ngcontent-%COMP%] {\n  background-color: #777777;\n}\n\n.c100.green[_ngcontent-%COMP%]   .bar[_ngcontent-%COMP%], .c100.green[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%] {\n  border-color: greenyellow !important;\n}\n\n.c100.blue[_ngcontent-%COMP%] {\n  background-color: #777777;\n}\n\n.c100.blue[_ngcontent-%COMP%]   .bar[_ngcontent-%COMP%], .c100.blue[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%] {\n  border-color: aquamarine !important;\n}\n\n.c100.red[_ngcontent-%COMP%] {\n  background-color: transparent;\n}\n\n.c100.red[_ngcontent-%COMP%]   .bar[_ngcontent-%COMP%], .c100.red[_ngcontent-%COMP%]   .fill[_ngcontent-%COMP%] {\n  border-color: red !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcGV0ZW5jZS9DOlxcVXNlcnNcXGh1XFxXZWJzdG9ybVByb2plY3RzXFxzaXRlcG9ydGZvbGlvXFxzaXRlcG9ydGZvbGlvWVQvc3JjXFxhcHBcXGNvbXBldGVuY2VcXGNvbXBldGVuY2UuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBldGVuY2UvY29tcGV0ZW5jZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGNBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQ0NGOztBREVBO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDQ0Y7O0FERUE7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7QUNDRjs7QURFQTtFQUNFLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FDQ0Y7O0FERUE7RUFDRSxZQUFBO0FDQ0Y7O0FERUE7RUFDRSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtBQ0NGOztBREdBO0VBQ0Usa0RBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSx5R0FBQTtVQUFBLGlHQUFBO0FDQUY7O0FER0E7RUFDRSxvREFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtFQUNBLHlHQUFBO1VBQUEsaUdBQUE7QUNBRjs7QURHQTtFQUNFLGlEQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EseUdBQUE7VUFBQSxpR0FBQTtBQ0FGOztBREdBO0VBQ0UsOENBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSx5R0FBQTtVQUFBLGlHQUFBO0FDQUY7O0FER0E7RUFDRSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUNBRjs7QURHQTtFQUNFLFlBQUE7RUFDQSxZQUFBO0VBQ0Esd0JBQUE7RUFDQSxtQkFBQTtBQ0FGOztBREdBO0VBQ0UsVUFBQTtFQUNBLFlBQUE7RUFDQSx3QkFBQTtFQUNBLG1CQUFBO0FDQUY7O0FER0E7RUFDRSxVQUFBO0VBQ0EsWUFBQTtFQUNBLHdCQUFBO0VBQ0EsbUJBQUE7QUNBRjs7QURHQTtFQUNFLFVBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtBQ0FGOztBREdBO0VBQ0UsVUFBQTtFQUNBLFlBQUE7RUFDQSxtQ0FBQTtFQUNBLG1CQUFBO0FDQUY7O0FER0E7RUFDRSxlQUFBO0VBQ0EsY0FBQTtBQ0FGOztBREdBO0VBQ0UsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLHVCQUFBO0VBQ0Esb0JBQUE7QUNBRjs7QURHQTtFQUNFLGVBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSw2QkFBQTtBQ0FGOztBREdBO0VBQ0Usa0NBQUE7QUNBRjs7QURHQTtFQUNFLGtCQUFBO0VBQ0EsNEJBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtFQUNBLGdDQUFBO0VBQ0Esa0JBQUE7QUNBRjs7QURJQTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUtBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7QUNERjs7QURJQTtFQUdFLHVCQUFBO0FDREY7O0FESUE7RUFDRSxXQUFBO0VBQ0EsY0FBQTtBQ0RGOztBRElBO0VBQ0UsZ0JBQUE7QUNERjs7QURJQTtFQUNFLGVBQUE7QUNERjs7QURJQTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQ0RGOztBREtBO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0FDRkY7O0FETUE7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsZ0NBQUE7QUNIRjs7QURNQTtFQUtFLDJCQUFBO0FDSEY7O0FETUE7RUFLRSwyQkFBQTtBQ0hGOztBRE1BO0VBS0UseUJBQUE7QUNIRjs7QURNQTtFQUNFLHlCQUFBO0FDSEY7O0FETUE7O0VBRUUsb0NBQUE7QUNIRjs7QURNQTtFQUNFLHlCQUFBO0FDSEY7O0FETUE7O0VBRUUsbUNBQUE7QUNIRjs7QURNQTtFQUNFLDZCQUFBO0FDSEY7O0FETUE7O0VBRUUsNEJBQUE7QUNIRiIsImZpbGUiOiJzcmMvYXBwL2NvbXBldGVuY2UvY29tcGV0ZW5jZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImgyIHtcclxuICBjb2xvcjogI2ZmZmZmZjtcclxuICBwYWRkaW5nOiAxMDBweDtcclxuICBmb250LXNpemU6IDQwcHg7XHJcbn1cclxuXHJcbmgzIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgY29sb3I6ICMwMGNhZjQ7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtc2l6ZTogMjZweDtcclxufVxyXG5cclxuLmNvbXBldGVuY2Vjb250YWluZXIge1xyXG4gIHdpZHRoOiA3MjBweDtcclxuICBtYXJnaW46IDAgYXV0byAwO1xyXG59XHJcblxyXG4ubGFuZ3VlIHtcclxuICBtYXJnaW46IDAgMCAxMDBweCAwO1xyXG4gIHdpZHRoOiA3MjBweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbi5sYW5ndWUgLnNsaWNlIHtcclxuICBmbGV4LWdyb3c6IDA7XHJcbn1cclxuXHJcbi5za2lsLWJveCB7XHJcbiAgbWFyZ2luOiA1MHB4IGF1dG8gMDtcclxuICB3aWR0aDogNzIwcHg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcblxyXG4uaWNvbi1qcyB7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vYXNzZXRzL2ljb25zOC1qYXZhc2NyaXB0LTEwMC5wbmdcIik7XHJcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbiAgd2lkdGg6IDEwMHB4O1xyXG4gIGZpbHRlcjogaW52ZXJ0KDg4JSkgc2VwaWEoMTAwJSkgc2F0dXJhdGUoMjkyOSUpIGh1ZS1yb3RhdGUoMTA0ZGVnKSBicmlnaHRuZXNzKDk5JSkgY29udHJhc3QoMTA0JSk7XHJcbn1cclxuXHJcbi5pY29uLXJlYWN0IHtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvaWNvbnM4LXJlYWN0LW5hdGl2ZS0xMDAucG5nXCIpO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICBoZWlnaHQ6IDEwMHB4O1xyXG4gIHdpZHRoOiAxMDBweDtcclxuICBmaWx0ZXI6IGludmVydCg4OCUpIHNlcGlhKDEwMCUpIHNhdHVyYXRlKDI5MjklKSBodWUtcm90YXRlKDEwNGRlZykgYnJpZ2h0bmVzcyg5OSUpIGNvbnRyYXN0KDEwNCUpO1xyXG59XHJcblxyXG4uaWNvbi1hbmd1bGFyIHtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvaWNvbnM4LWFuZ3VsYXJqcy0xMDAucG5nXCIpO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICBoZWlnaHQ6IDEwMHB4O1xyXG4gIHdpZHRoOiAxMDBweDtcclxuICBmaWx0ZXI6IGludmVydCg4OCUpIHNlcGlhKDEwMCUpIHNhdHVyYXRlKDI5MjklKSBodWUtcm90YXRlKDEwNGRlZykgYnJpZ2h0bmVzcyg5OSUpIGNvbnRyYXN0KDEwNCUpO1xyXG59XHJcblxyXG4uaWNvbi1ub2RlIHtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvaWNvbnM4LW5vZGVqcy0xMDAucG5nXCIpO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICBoZWlnaHQ6IDEwMHB4O1xyXG4gIHdpZHRoOiAxMDBweDtcclxuICBmaWx0ZXI6IGludmVydCg4OCUpIHNlcGlhKDEwMCUpIHNhdHVyYXRlKDI5MjklKSBodWUtcm90YXRlKDEwNGRlZykgYnJpZ2h0bmVzcyg5OSUpIGNvbnRyYXN0KDEwNCUpO1xyXG59XHJcblxyXG4uc2tpbC10ZXh0IHtcclxuICB3aWR0aDogNTgwcHg7XHJcbiAgbWFyZ2luOiAzMHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLnNraWxiYXIge1xyXG4gIHdpZHRoOiA0NzBweDtcclxuICBoZWlnaHQ6IDIwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogc2lsdmVyO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbn1cclxuXHJcbi5za2lsYmFyLWpzIHtcclxuICB3aWR0aDogOTAlO1xyXG4gIGhlaWdodDogMjBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBvcmFuZ2U7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxufVxyXG5cclxuLnNraWxiYXItciB7XHJcbiAgd2lkdGg6IDcwJTtcclxuICBoZWlnaHQ6IDIwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogeWVsbG93O1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbn1cclxuXHJcbi5za2lsYmFyLWEge1xyXG4gIHdpZHRoOiA1MCU7XHJcbiAgaGVpZ2h0OiAyMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJlZDtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG59XHJcblxyXG4uc2tpbGJhci1ub2RlIHtcclxuICB3aWR0aDogODUlO1xyXG4gIGhlaWdodDogMjBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBtZWRpdW1zcHJpbmdncmVlbjtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG59XHJcblxyXG4uc2tpbC10ZXh0IHAge1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBjb2xvcjogI2ZmZmZmZjtcclxufVxyXG5cclxuLnNraWwtb3RoZXItYm94IHtcclxuICBtYXJnaW46IDUwcHggYXV0byAwO1xyXG4gIHdpZHRoOiA3MjBweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgZmxleC13cmFwOiB3cmFwO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIHBhZGRpbmctYm90dG9tOiA2MHB4O1xyXG59XHJcblxyXG4uc2tpbC1vdGhlcnMge1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgcGFkZGluZzogMTBweCAyMHB4O1xyXG4gIG1hcmdpbjogMTBweCAyMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDQwcHg7XHJcbiAgYm9yZGVyOiBzb2xpZCAycHggIzAwY2FmNDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxufVxyXG5cclxuLnJlY3QtYXV0bywgLmMxMDAucDUxIC5zbGljZSwgLmMxMDAucDUyIC5zbGljZSwgLmMxMDAucDUzIC5zbGljZSwgLmMxMDAucDU0IC5zbGljZSwgLmMxMDAucDU1IC5zbGljZSwgLmMxMDAucDU2IC5zbGljZSwgLmMxMDAucDU3IC5zbGljZSwgLmMxMDAucDU4IC5zbGljZSwgLmMxMDAucDU5IC5zbGljZSwgLmMxMDAucDYwIC5zbGljZSwgLmMxMDAucDYxIC5zbGljZSwgLmMxMDAucDYyIC5zbGljZSwgLmMxMDAucDYzIC5zbGljZSwgLmMxMDAucDY0IC5zbGljZSwgLmMxMDAucDY1IC5zbGljZSwgLmMxMDAucDY2IC5zbGljZSwgLmMxMDAucDY3IC5zbGljZSwgLmMxMDAucDY4IC5zbGljZSwgLmMxMDAucDY5IC5zbGljZSwgLmMxMDAucDcwIC5zbGljZSwgLmMxMDAucDcxIC5zbGljZSwgLmMxMDAucDcyIC5zbGljZSwgLmMxMDAucDczIC5zbGljZSwgLmMxMDAucDc0IC5zbGljZSwgLmMxMDAucDc1IC5zbGljZSwgLmMxMDAucDc2IC5zbGljZSwgLmMxMDAucDc3IC5zbGljZSwgLmMxMDAucDc4IC5zbGljZSwgLmMxMDAucDc5IC5zbGljZSwgLmMxMDAucDgwIC5zbGljZSwgLmMxMDAucDgxIC5zbGljZSwgLmMxMDAucDgyIC5zbGljZSwgLmMxMDAucDgzIC5zbGljZSwgLmMxMDAucDg0IC5zbGljZSwgLmMxMDAucDg1IC5zbGljZSwgLmMxMDAucDg2IC5zbGljZSwgLmMxMDAucDg3IC5zbGljZSwgLmMxMDAucDg4IC5zbGljZSwgLmMxMDAucDg5IC5zbGljZSwgLmMxMDAucDkwIC5zbGljZSwgLmMxMDAucDkxIC5zbGljZSwgLmMxMDAucDkyIC5zbGljZSwgLmMxMDAucDkzIC5zbGljZSwgLmMxMDAucDk0IC5zbGljZSwgLmMxMDAucDk1IC5zbGljZSwgLmMxMDAucDk2IC5zbGljZSwgLmMxMDAucDk3IC5zbGljZSwgLmMxMDAucDk4IC5zbGljZSwgLmMxMDAucDk5IC5zbGljZSwgLmMxMDAucDEwMCAuc2xpY2Uge1xyXG4gIGNsaXA6IHJlY3QoYXV0bywgYXV0bywgYXV0bywgYXV0byk7XHJcbn1cclxuXHJcbi5waWUsIC5jMTAwIC5iYXIsIC5jMTAwLnA1MSAuZmlsbCwgLmMxMDAucDUyIC5maWxsLCAuYzEwMC5wNTMgLmZpbGwsIC5jMTAwLnA1NCAuZmlsbCwgLmMxMDAucDU1IC5maWxsLCAuYzEwMC5wNTYgLmZpbGwsIC5jMTAwLnA1NyAuZmlsbCwgLmMxMDAucDU4IC5maWxsLCAuYzEwMC5wNTkgLmZpbGwsIC5jMTAwLnA2MCAuZmlsbCwgLmMxMDAucDYxIC5maWxsLCAuYzEwMC5wNjIgLmZpbGwsIC5jMTAwLnA2MyAuZmlsbCwgLmMxMDAucDY0IC5maWxsLCAuYzEwMC5wNjUgLmZpbGwsIC5jMTAwLnA2NiAuZmlsbCwgLmMxMDAucDY3IC5maWxsLCAuYzEwMC5wNjggLmZpbGwsIC5jMTAwLnA2OSAuZmlsbCwgLmMxMDAucDcwIC5maWxsLCAuYzEwMC5wNzEgLmZpbGwsIC5jMTAwLnA3MiAuZmlsbCwgLmMxMDAucDczIC5maWxsLCAuYzEwMC5wNzQgLmZpbGwsIC5jMTAwLnA3NSAuZmlsbCwgLmMxMDAucDc2IC5maWxsLCAuYzEwMC5wNzcgLmZpbGwsIC5jMTAwLnA3OCAuZmlsbCwgLmMxMDAucDc5IC5maWxsLCAuYzEwMC5wODAgLmZpbGwsIC5jMTAwLnA4MSAuZmlsbCwgLmMxMDAucDgyIC5maWxsLCAuYzEwMC5wODMgLmZpbGwsIC5jMTAwLnA4NCAuZmlsbCwgLmMxMDAucDg1IC5maWxsLCAuYzEwMC5wODYgLmZpbGwsIC5jMTAwLnA4NyAuZmlsbCwgLmMxMDAucDg4IC5maWxsLCAuYzEwMC5wODkgLmZpbGwsIC5jMTAwLnA5MCAuZmlsbCwgLmMxMDAucDkxIC5maWxsLCAuYzEwMC5wOTIgLmZpbGwsIC5jMTAwLnA5MyAuZmlsbCwgLmMxMDAucDk0IC5maWxsLCAuYzEwMC5wOTUgLmZpbGwsIC5jMTAwLnA5NiAuZmlsbCwgLmMxMDAucDk3IC5maWxsLCAuYzEwMC5wOTggLmZpbGwsIC5jMTAwLnA5OSAuZmlsbCwgLmMxMDAucDEwMCAuZmlsbCB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGJvcmRlcjogMC4wOGVtIHNvbGlkICMzMDdiYmI7XHJcbiAgd2lkdGg6IDAuODRlbTtcclxuICBoZWlnaHQ6IDAuODRlbTtcclxuICBjbGlwOiByZWN0KDBlbSwgMC41ZW0sIDFlbSwgMGVtKTtcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcblxyXG59XHJcblxyXG4uYzEwMCB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGZvbnQtc2l6ZTogMTIwcHg7XHJcbiAgd2lkdGg6IDFlbTtcclxuICBoZWlnaHQ6IDFlbTtcclxuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAtbW96LWJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAtbXMtYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gIC1vLWJvcmRlci1yYWRpdXM6IDUwJTtcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgbWFyZ2luOiA1MHB4IDMwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogZ3JheTtcclxufVxyXG5cclxuLmMxMDAgKiwgLmMxMDAgKjpiZWZvcmUsIC5jMTAwICo6YWZ0ZXIge1xyXG4gIC13ZWJraXQtYm94LXNpemluZzogY29udGVudC1ib3g7XHJcbiAgLW1vei1ib3gtc2l6aW5nOiBjb250ZW50LWJveDtcclxuICBib3gtc2l6aW5nOiBjb250ZW50LWJveDtcclxufVxyXG5cclxuLmMxMDAuY2VudGVyIHtcclxuICBmbG9hdDogbm9uZTtcclxuICBtYXJnaW46IDAgYXV0bztcclxufVxyXG5cclxuLmMxMDAuYmlnIHtcclxuICBmb250LXNpemU6IDE3MHB4O1xyXG59XHJcblxyXG4uYzEwMC5zbWFsbCB7XHJcbiAgZm9udC1zaXplOiA4MHB4O1xyXG59XHJcblxyXG4uYzEwMCA+IHNwYW4ge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDUwJTtcclxuICBsZWZ0OiA1MCU7XHJcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgei1pbmRleDogMTtcclxuICBmb250LXNpemU6IDAuMTVlbTtcclxuICBjb2xvcjogI0ZGRkZGRjtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuXHJcbn1cclxuXHJcbi5jMTAwOmFmdGVyIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiAwLjA4ZW07XHJcbiAgbGVmdDogMC4wOGVtO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIGNvbnRlbnQ6IFwiIFwiO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDM0ZGEyO1xyXG4gIHdpZHRoOiAwLjg0ZW07XHJcbiAgaGVpZ2h0OiAwLjg0ZW07XHJcblxyXG59XHJcblxyXG4uYzEwMCAuc2xpY2Uge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB3aWR0aDogMWVtO1xyXG4gIGhlaWdodDogMWVtO1xyXG4gIGNsaXA6IHJlY3QoMGVtLCAxZW0sIDFlbSwgMC41ZW0pO1xyXG59XHJcblxyXG4uYzEwMC5wNTEgLmJhciB7XHJcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxODMuNmRlZyk7XHJcbiAgLW1vei10cmFuc2Zvcm06IHJvdGF0ZSgxODMuNmRlZyk7XHJcbiAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDE4My42ZGVnKTtcclxuICAtby10cmFuc2Zvcm06IHJvdGF0ZSgxODMuNmRlZyk7XHJcbiAgdHJhbnNmb3JtOiByb3RhdGUoMTgzLjZkZWcpO1xyXG59XHJcblxyXG4uYzEwMC5wNzcgLmJhciB7XHJcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgyNzcuMmRlZyk7XHJcbiAgLW1vei10cmFuc2Zvcm06IHJvdGF0ZSgyNzcuMmRlZyk7XHJcbiAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDI3Ny4yZGVnKTtcclxuICAtby10cmFuc2Zvcm06IHJvdGF0ZSgyNzcuMmRlZyk7XHJcbiAgdHJhbnNmb3JtOiByb3RhdGUoMjc3LjJkZWcpO1xyXG59XHJcblxyXG4uYzEwMC5wMTAwIC5iYXIge1xyXG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTtcclxuICAtbW96LXRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7XHJcbiAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7XHJcbiAgLW8tdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTtcclxuICB0cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpO1xyXG59XHJcblxyXG4uYzEwMC5ncmVlbiB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzc3Nzc3NztcclxufVxyXG5cclxuLmMxMDAuZ3JlZW4gLmJhcixcclxuLmMxMDAuZ3JlZW4gLmZpbGwge1xyXG4gIGJvcmRlci1jb2xvcjogZ3JlZW55ZWxsb3cgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmMxMDAuYmx1ZSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzc3Nzc3NztcclxufVxyXG5cclxuLmMxMDAuYmx1ZSAuYmFyLFxyXG4uYzEwMC5ibHVlIC5maWxsIHtcclxuICBib3JkZXItY29sb3I6IGFxdWFtYXJpbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmMxMDAucmVkIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxufVxyXG5cclxuLmMxMDAucmVkIC5iYXIsXHJcbi5jMTAwLnJlZCAuZmlsbCB7XHJcbiAgYm9yZGVyLWNvbG9yOiByZWQgIWltcG9ydGFudDtcclxufVxyXG4iLCJoMiB7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBwYWRkaW5nOiAxMDBweDtcbiAgZm9udC1zaXplOiA0MHB4O1xufVxuXG5oMyB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgY29sb3I6ICMwMGNhZjQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAyNnB4O1xufVxuXG4uY29tcGV0ZW5jZWNvbnRhaW5lciB7XG4gIHdpZHRoOiA3MjBweDtcbiAgbWFyZ2luOiAwIGF1dG8gMDtcbn1cblxuLmxhbmd1ZSB7XG4gIG1hcmdpbjogMCAwIDEwMHB4IDA7XG4gIHdpZHRoOiA3MjBweDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5sYW5ndWUgLnNsaWNlIHtcbiAgZmxleC1ncm93OiAwO1xufVxuXG4uc2tpbC1ib3gge1xuICBtYXJnaW46IDUwcHggYXV0byAwO1xuICB3aWR0aDogNzIwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbi5pY29uLWpzIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vYXNzZXRzL2ljb25zOC1qYXZhc2NyaXB0LTEwMC5wbmdcIik7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGhlaWdodDogMTAwcHg7XG4gIHdpZHRoOiAxMDBweDtcbiAgZmlsdGVyOiBpbnZlcnQoODglKSBzZXBpYSgxMDAlKSBzYXR1cmF0ZSgyOTI5JSkgaHVlLXJvdGF0ZSgxMDRkZWcpIGJyaWdodG5lc3MoOTklKSBjb250cmFzdCgxMDQlKTtcbn1cblxuLmljb24tcmVhY3Qge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvaWNvbnM4LXJlYWN0LW5hdGl2ZS0xMDAucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBoZWlnaHQ6IDEwMHB4O1xuICB3aWR0aDogMTAwcHg7XG4gIGZpbHRlcjogaW52ZXJ0KDg4JSkgc2VwaWEoMTAwJSkgc2F0dXJhdGUoMjkyOSUpIGh1ZS1yb3RhdGUoMTA0ZGVnKSBicmlnaHRuZXNzKDk5JSkgY29udHJhc3QoMTA0JSk7XG59XG5cbi5pY29uLWFuZ3VsYXIge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvaWNvbnM4LWFuZ3VsYXJqcy0xMDAucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBoZWlnaHQ6IDEwMHB4O1xuICB3aWR0aDogMTAwcHg7XG4gIGZpbHRlcjogaW52ZXJ0KDg4JSkgc2VwaWEoMTAwJSkgc2F0dXJhdGUoMjkyOSUpIGh1ZS1yb3RhdGUoMTA0ZGVnKSBicmlnaHRuZXNzKDk5JSkgY29udHJhc3QoMTA0JSk7XG59XG5cbi5pY29uLW5vZGUge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvaWNvbnM4LW5vZGVqcy0xMDAucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBoZWlnaHQ6IDEwMHB4O1xuICB3aWR0aDogMTAwcHg7XG4gIGZpbHRlcjogaW52ZXJ0KDg4JSkgc2VwaWEoMTAwJSkgc2F0dXJhdGUoMjkyOSUpIGh1ZS1yb3RhdGUoMTA0ZGVnKSBicmlnaHRuZXNzKDk5JSkgY29udHJhc3QoMTA0JSk7XG59XG5cbi5za2lsLXRleHQge1xuICB3aWR0aDogNTgwcHg7XG4gIG1hcmdpbjogMzBweDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uc2tpbGJhciB7XG4gIHdpZHRoOiA0NzBweDtcbiAgaGVpZ2h0OiAyMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBzaWx2ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG5cbi5za2lsYmFyLWpzIHtcbiAgd2lkdGg6IDkwJTtcbiAgaGVpZ2h0OiAyMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBvcmFuZ2U7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG5cbi5za2lsYmFyLXIge1xuICB3aWR0aDogNzAlO1xuICBoZWlnaHQ6IDIwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHllbGxvdztcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbn1cblxuLnNraWxiYXItYSB7XG4gIHdpZHRoOiA1MCU7XG4gIGhlaWdodDogMjBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuXG4uc2tpbGJhci1ub2RlIHtcbiAgd2lkdGg6IDg1JTtcbiAgaGVpZ2h0OiAyMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBtZWRpdW1zcHJpbmdncmVlbjtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbn1cblxuLnNraWwtdGV4dCBwIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBjb2xvcjogI2ZmZmZmZjtcbn1cblxuLnNraWwtb3RoZXItYm94IHtcbiAgbWFyZ2luOiA1MHB4IGF1dG8gMDtcbiAgd2lkdGg6IDcyMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBmbGV4LXdyYXA6IHdyYXA7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBwYWRkaW5nLWJvdHRvbTogNjBweDtcbn1cblxuLnNraWwtb3RoZXJzIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDEwcHggMjBweDtcbiAgbWFyZ2luOiAxMHB4IDIwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDQwcHg7XG4gIGJvcmRlcjogc29saWQgMnB4ICMwMGNhZjQ7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xufVxuXG4ucmVjdC1hdXRvLCAuYzEwMC5wNTEgLnNsaWNlLCAuYzEwMC5wNTIgLnNsaWNlLCAuYzEwMC5wNTMgLnNsaWNlLCAuYzEwMC5wNTQgLnNsaWNlLCAuYzEwMC5wNTUgLnNsaWNlLCAuYzEwMC5wNTYgLnNsaWNlLCAuYzEwMC5wNTcgLnNsaWNlLCAuYzEwMC5wNTggLnNsaWNlLCAuYzEwMC5wNTkgLnNsaWNlLCAuYzEwMC5wNjAgLnNsaWNlLCAuYzEwMC5wNjEgLnNsaWNlLCAuYzEwMC5wNjIgLnNsaWNlLCAuYzEwMC5wNjMgLnNsaWNlLCAuYzEwMC5wNjQgLnNsaWNlLCAuYzEwMC5wNjUgLnNsaWNlLCAuYzEwMC5wNjYgLnNsaWNlLCAuYzEwMC5wNjcgLnNsaWNlLCAuYzEwMC5wNjggLnNsaWNlLCAuYzEwMC5wNjkgLnNsaWNlLCAuYzEwMC5wNzAgLnNsaWNlLCAuYzEwMC5wNzEgLnNsaWNlLCAuYzEwMC5wNzIgLnNsaWNlLCAuYzEwMC5wNzMgLnNsaWNlLCAuYzEwMC5wNzQgLnNsaWNlLCAuYzEwMC5wNzUgLnNsaWNlLCAuYzEwMC5wNzYgLnNsaWNlLCAuYzEwMC5wNzcgLnNsaWNlLCAuYzEwMC5wNzggLnNsaWNlLCAuYzEwMC5wNzkgLnNsaWNlLCAuYzEwMC5wODAgLnNsaWNlLCAuYzEwMC5wODEgLnNsaWNlLCAuYzEwMC5wODIgLnNsaWNlLCAuYzEwMC5wODMgLnNsaWNlLCAuYzEwMC5wODQgLnNsaWNlLCAuYzEwMC5wODUgLnNsaWNlLCAuYzEwMC5wODYgLnNsaWNlLCAuYzEwMC5wODcgLnNsaWNlLCAuYzEwMC5wODggLnNsaWNlLCAuYzEwMC5wODkgLnNsaWNlLCAuYzEwMC5wOTAgLnNsaWNlLCAuYzEwMC5wOTEgLnNsaWNlLCAuYzEwMC5wOTIgLnNsaWNlLCAuYzEwMC5wOTMgLnNsaWNlLCAuYzEwMC5wOTQgLnNsaWNlLCAuYzEwMC5wOTUgLnNsaWNlLCAuYzEwMC5wOTYgLnNsaWNlLCAuYzEwMC5wOTcgLnNsaWNlLCAuYzEwMC5wOTggLnNsaWNlLCAuYzEwMC5wOTkgLnNsaWNlLCAuYzEwMC5wMTAwIC5zbGljZSB7XG4gIGNsaXA6IHJlY3QoYXV0bywgYXV0bywgYXV0bywgYXV0byk7XG59XG5cbi5waWUsIC5jMTAwIC5iYXIsIC5jMTAwLnA1MSAuZmlsbCwgLmMxMDAucDUyIC5maWxsLCAuYzEwMC5wNTMgLmZpbGwsIC5jMTAwLnA1NCAuZmlsbCwgLmMxMDAucDU1IC5maWxsLCAuYzEwMC5wNTYgLmZpbGwsIC5jMTAwLnA1NyAuZmlsbCwgLmMxMDAucDU4IC5maWxsLCAuYzEwMC5wNTkgLmZpbGwsIC5jMTAwLnA2MCAuZmlsbCwgLmMxMDAucDYxIC5maWxsLCAuYzEwMC5wNjIgLmZpbGwsIC5jMTAwLnA2MyAuZmlsbCwgLmMxMDAucDY0IC5maWxsLCAuYzEwMC5wNjUgLmZpbGwsIC5jMTAwLnA2NiAuZmlsbCwgLmMxMDAucDY3IC5maWxsLCAuYzEwMC5wNjggLmZpbGwsIC5jMTAwLnA2OSAuZmlsbCwgLmMxMDAucDcwIC5maWxsLCAuYzEwMC5wNzEgLmZpbGwsIC5jMTAwLnA3MiAuZmlsbCwgLmMxMDAucDczIC5maWxsLCAuYzEwMC5wNzQgLmZpbGwsIC5jMTAwLnA3NSAuZmlsbCwgLmMxMDAucDc2IC5maWxsLCAuYzEwMC5wNzcgLmZpbGwsIC5jMTAwLnA3OCAuZmlsbCwgLmMxMDAucDc5IC5maWxsLCAuYzEwMC5wODAgLmZpbGwsIC5jMTAwLnA4MSAuZmlsbCwgLmMxMDAucDgyIC5maWxsLCAuYzEwMC5wODMgLmZpbGwsIC5jMTAwLnA4NCAuZmlsbCwgLmMxMDAucDg1IC5maWxsLCAuYzEwMC5wODYgLmZpbGwsIC5jMTAwLnA4NyAuZmlsbCwgLmMxMDAucDg4IC5maWxsLCAuYzEwMC5wODkgLmZpbGwsIC5jMTAwLnA5MCAuZmlsbCwgLmMxMDAucDkxIC5maWxsLCAuYzEwMC5wOTIgLmZpbGwsIC5jMTAwLnA5MyAuZmlsbCwgLmMxMDAucDk0IC5maWxsLCAuYzEwMC5wOTUgLmZpbGwsIC5jMTAwLnA5NiAuZmlsbCwgLmMxMDAucDk3IC5maWxsLCAuYzEwMC5wOTggLmZpbGwsIC5jMTAwLnA5OSAuZmlsbCwgLmMxMDAucDEwMCAuZmlsbCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm9yZGVyOiAwLjA4ZW0gc29saWQgIzMwN2JiYjtcbiAgd2lkdGg6IDAuODRlbTtcbiAgaGVpZ2h0OiAwLjg0ZW07XG4gIGNsaXA6IHJlY3QoMGVtLCAwLjVlbSwgMWVtLCAwZW0pO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG59XG5cbi5jMTAwIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBmb250LXNpemU6IDEyMHB4O1xuICB3aWR0aDogMWVtO1xuICBoZWlnaHQ6IDFlbTtcbiAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA1MCU7XG4gIC1tb3otYm9yZGVyLXJhZGl1czogNTAlO1xuICAtbXMtYm9yZGVyLXJhZGl1czogNTAlO1xuICAtby1ib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgZmxvYXQ6IGxlZnQ7XG4gIG1hcmdpbjogNTBweCAzMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBncmF5O1xufVxuXG4uYzEwMCAqLCAuYzEwMCAqOmJlZm9yZSwgLmMxMDAgKjphZnRlciB7XG4gIC13ZWJraXQtYm94LXNpemluZzogY29udGVudC1ib3g7XG4gIC1tb3otYm94LXNpemluZzogY29udGVudC1ib3g7XG4gIGJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xufVxuXG4uYzEwMC5jZW50ZXIge1xuICBmbG9hdDogbm9uZTtcbiAgbWFyZ2luOiAwIGF1dG87XG59XG5cbi5jMTAwLmJpZyB7XG4gIGZvbnQtc2l6ZTogMTcwcHg7XG59XG5cbi5jMTAwLnNtYWxsIHtcbiAgZm9udC1zaXplOiA4MHB4O1xufVxuXG4uYzEwMCA+IHNwYW4ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNTAlO1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICB6LWluZGV4OiAxO1xuICBmb250LXNpemU6IDAuMTVlbTtcbiAgY29sb3I6ICNGRkZGRkY7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG59XG5cbi5jMTAwOmFmdGVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDAuMDhlbTtcbiAgbGVmdDogMC4wOGVtO1xuICBkaXNwbGF5OiBibG9jaztcbiAgY29udGVudDogXCIgXCI7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzQzNGRhMjtcbiAgd2lkdGg6IDAuODRlbTtcbiAgaGVpZ2h0OiAwLjg0ZW07XG59XG5cbi5jMTAwIC5zbGljZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDFlbTtcbiAgaGVpZ2h0OiAxZW07XG4gIGNsaXA6IHJlY3QoMGVtLCAxZW0sIDFlbSwgMC41ZW0pO1xufVxuXG4uYzEwMC5wNTEgLmJhciB7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTgzLjZkZWcpO1xuICAtbW96LXRyYW5zZm9ybTogcm90YXRlKDE4My42ZGVnKTtcbiAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDE4My42ZGVnKTtcbiAgLW8tdHJhbnNmb3JtOiByb3RhdGUoMTgzLjZkZWcpO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgxODMuNmRlZyk7XG59XG5cbi5jMTAwLnA3NyAuYmFyIHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgyNzcuMmRlZyk7XG4gIC1tb3otdHJhbnNmb3JtOiByb3RhdGUoMjc3LjJkZWcpO1xuICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoMjc3LjJkZWcpO1xuICAtby10cmFuc2Zvcm06IHJvdGF0ZSgyNzcuMmRlZyk7XG4gIHRyYW5zZm9ybTogcm90YXRlKDI3Ny4yZGVnKTtcbn1cblxuLmMxMDAucDEwMCAuYmFyIHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpO1xuICAtbW96LXRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7XG4gIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpO1xuICAtby10cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpO1xufVxuXG4uYzEwMC5ncmVlbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM3Nzc3Nzc7XG59XG5cbi5jMTAwLmdyZWVuIC5iYXIsXG4uYzEwMC5ncmVlbiAuZmlsbCB7XG4gIGJvcmRlci1jb2xvcjogZ3JlZW55ZWxsb3cgIWltcG9ydGFudDtcbn1cblxuLmMxMDAuYmx1ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM3Nzc3Nzc7XG59XG5cbi5jMTAwLmJsdWUgLmJhcixcbi5jMTAwLmJsdWUgLmZpbGwge1xuICBib3JkZXItY29sb3I6IGFxdWFtYXJpbmUgIWltcG9ydGFudDtcbn1cblxuLmMxMDAucmVkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG59XG5cbi5jMTAwLnJlZCAuYmFyLFxuLmMxMDAucmVkIC5maWxsIHtcbiAgYm9yZGVyLWNvbG9yOiByZWQgIWltcG9ydGFudDtcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CompetenceComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-competence',
                templateUrl: './competence.component.html',
                styleUrls: ['./competence.component.scss']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/contact/contact.component.ts":
/*!**********************************************!*\
  !*** ./src/app/contact/contact.component.ts ***!
  \**********************************************/
/*! exports provided: ContactComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactComponent", function() { return ContactComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _contact2_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./contact2.service */ "./src/app/contact/contact2.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");






function ContactComponent_span_8_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " * Champ obligatoire. ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ContactComponent_span_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ContactComponent_span_8_span_1_Template, 2, 0, "span", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.nameControl.hasError("required"));
} }
function ContactComponent_span_14_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " * Champ obligatoire. ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ContactComponent_span_14_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "* Mettez sous la forme d'email. ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ContactComponent_span_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ContactComponent_span_14_span_1_Template, 2, 0, "span", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, ContactComponent_span_14_span_2_Template, 2, 0, "span", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.emailControl.hasError("required"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.emailControl.hasError("email"));
} }
function ContactComponent_span_25_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " * Champ obligatoire. ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ContactComponent_span_25_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ContactComponent_span_25_span_1_Template, 2, 0, "span", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r2.bodyControl.hasError("required"));
} }
class ContactComponent {
    constructor(fb, contactService) {
        this.fb = fb;
        this.contactService = contactService;
        this.contactform = this.fb.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]],
            object: [''],
            body: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
        });
    }
    get nameControl() {
        return this.contactform.get('name');
    }
    get emailControl() {
        return this.contactform.get('email');
    }
    get objectControl() {
        return this.contactform.get('object');
    }
    get bodyControl() {
        return this.contactform.get('body');
    }
    viewconfirm() {
        document.getElementById("modalconfirm").style.display = "block";
    }
    sendform() {
        this.contactService.addnewmail(this.contactform.value).subscribe();
        document.getElementById("modalsended").style.display = "block";
        console.log(this.contactform.value);
    }
    closeconfirm() {
        document.getElementById("modalconfirm").style.display = "none";
    }
    closesended() {
        document.getElementById("modalsended").style.display = "none";
    }
    onEvent(e) {
        e.preventDefault();
        e.stopPropagation();
    }
}
ContactComponent.ɵfac = function ContactComponent_Factory(t) { return new (t || ContactComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_contact2_service__WEBPACK_IMPORTED_MODULE_2__["ContactService"])); };
ContactComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ContactComponent, selectors: [["app-contact"]], decls: 74, vars: 9, consts: [[1, "formbox", 3, "formGroup", "ngSubmit"], [1, "formelement"], ["type", "text", "placeholder", "Nom d'exp\u00E9diteur", "formControlName", "name", "required", "", "autocomplete", "on"], [4, "ngIf"], ["type", "email", "placeholder", "Email", "formControlName", "email", "required", "", "autocomplete", "on"], ["type", "text", "placeholder", "Object:", "formControlName", "object", "autocomplete", "on"], ["type", "text", "placeholder", "Message", "formControlName", "body", "required", "", "autocomplete", "on"], [1, "btnconfirm"], ["type", "submit", 3, "disabled", "click"], ["id", "modalconfirm"], [1, "modalmain"], [1, "modalbtn"], [3, "click"], ["id", "modalsended"], [1, "modalmain2"], [1, "linkinconbox"], [1, "linkicon"], ["href", "mailto:dezidezi9495@outlook.com"], ["src", "../assets/icons8-composing-mail-100.png", "alt", "", 1, "linkimg"], ["href", "https://www.linkedin.com/in/takahiro-y-551a1918b/"], ["src", "../assets/icons8-linkedin-100.png", "alt", "", 1, "linkimg"], ["href", ""], ["src", "../assets/icons8-slack-new-100.png", "alt", "", 1, "linkimg"], ["href", "https://gitlab.com/Takahi"], ["src", "../assets/icons8-gitlab-100.png", "alt", "", 1, "linkimg"], ["href", "https://www.facebook.com/profile.php?id=100010351771579"], ["src", "../assets/icons8-facebook-old-100.png", "alt", "", 1, "linkimg"], ["href", "https://twitter.com/adreamtogalaxy"], ["src", "../assets/icons8-twitter-100.png", "alt", "", 1, "linkimg"], ["href", "https://www.instagram.com/takahoroo/"], ["src", "../assets/icons8-instagram-old-100.png", "alt", "", 1, "linkimg"]], template: function ContactComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "form", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function ContactComponent_Template_form_ngSubmit_0_listener() { return ctx.sendform(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "CONTACT");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Nom");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "input", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, ContactComponent_span_8_Template, 2, 1, "span", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "E-mail");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "input", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, ContactComponent_span_14_Template, 3, 2, "span", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Object");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "input", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Message");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "textarea", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](25, ContactComponent_span_25_Template, 2, 1, "span", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "button", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ContactComponent_Template_button_click_27_listener($event) { ctx.viewconfirm(); return ctx.onEvent($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, " Envoyer ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, " Confirmation Message ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ContactComponent_Template_button_click_41_listener($event) { ctx.closeconfirm(); return ctx.onEvent($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Retour");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ContactComponent_Template_button_click_43_listener($event) { ctx.sendform(); ctx.closeconfirm(); return ctx.onEvent($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "Envoyer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "Message est bien envoy\u00E9.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ContactComponent_Template_button_click_50_listener($event) { ctx.closesended(); return ctx.onEvent($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "OK");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "a", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "img", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "a", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](58, "img", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "a", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](61, "img", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "img", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "a", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](67, "img", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "a", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "img", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "a", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](73, "img", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.contactform);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.nameControl.invalid && (ctx.nameControl.dirty || ctx.nameControl.touched));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.emailControl.invalid && (ctx.emailControl.dirty || ctx.emailControl.touched));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.bodyControl.invalid && (ctx.bodyControl.dirty || ctx.bodyControl.touched));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", ctx.contactform.invalid || ctx.contactform.pristine);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("de: ", ctx.contactform.value.name, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.contactform.value.email);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.contactform.value.object);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.contactform.value.body);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RequiredValidator"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"]], styles: ["h2[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 100px;\n  font-size: 40px;\n}\n\n.formbox[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-content: center;\n  align-items: center;\n  font-size: 16px;\n}\n\n.formelement[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  color: white;\n  font-size: 20px;\n}\n\n.formelement[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  display: block;\n  font-size: 6px;\n  color: #fa8072;\n}\n\n.formelement[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n  display: block;\n  width: 400px;\n  border: none;\n  font-size: 16px;\n  line-height: 25px;\n  margin-bottom: 2px;\n  padding: 2px;\n}\n\n.formelement[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n  width: 400px;\n  min-height: 200px;\n  border: none;\n  font-size: 16px;\n  line-height: 25px;\n  margin-bottom: 2px;\n  padding: 2px;\n  word-break: normal;\n  font-family: Arial;\n}\n\n.btnconfirm[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  font-size: 20px;\n  padding: 6px 30px;\n  margin: 20px;\n  position: relative;\n  border-radius: 8px;\n  border: 2px solid royalblue;\n}\n\n#modalconfirm[_ngcontent-%COMP%] {\n  z-index: 980;\n  display: none;\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 120%;\n  background-color: rgba(0, 0, 0, 0.75);\n}\n\n#modalsended[_ngcontent-%COMP%] {\n  z-index: 990;\n  display: none;\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 120%;\n  background-color: rgba(0, 0, 0, 0.75);\n}\n\n.modalmain[_ngcontent-%COMP%] {\n  z-index: 999;\n  position: absolute;\n  top: 40%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  background-color: rgba(230, 230, 250, 0.8);\n  padding: 30px;\n  max-width: 400px;\n}\n\n.modalmain[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  padding: 6px 20px;\n  word-wrap: normal;\n  text-align: left;\n}\n\n.modalmain2[_ngcontent-%COMP%] {\n  z-index: 999;\n  position: absolute;\n  top: 40%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  background-color: rgba(230, 230, 250, 0.8);\n  padding: 30px;\n  max-width: 400px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-content: center;\n  align-items: center;\n}\n\n.modalbtn[_ngcontent-%COMP%] {\n  margin: 0 auto 0;\n}\n\n.modalbtn[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  font-size: 20px;\n  padding: 6px 30px;\n  margin: 20px;\n  position: relative;\n  border-radius: 8px;\n  border: 2px solid royalblue;\n}\n\n.linkinconbox[_ngcontent-%COMP%] {\n  margin: 50px;\n  display: flex;\n  flex-direction: row;\n}\n\n.linkicon[_ngcontent-%COMP%] {\n  -webkit-filter: invert(88%) sepia(61%) saturate(0%) hue-rotate(229deg) brightness(107%) contrast(101%);\n          filter: invert(88%) sepia(61%) saturate(0%) hue-rotate(229deg) brightness(107%) contrast(101%);\n  display: inline-block;\n  border-bottom: solid 2px transparent;\n}\n\n.linkicon[_ngcontent-%COMP%]:hover {\n  border-bottom: solid 2px black;\n}\n\n.linkimg[_ngcontent-%COMP%] {\n  width: 50px;\n  padding: 6px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29udGFjdC9DOlxcVXNlcnNcXGh1XFxXZWJzdG9ybVByb2plY3RzXFxzaXRlcG9ydGZvbGlvXFxzaXRlcG9ydGZvbGlvWVQvc3JjXFxhcHBcXGNvbnRhY3RcXGNvbnRhY3QuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbnRhY3QvY29udGFjdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGNBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQ0NGOztBREVBO0VBQ0UsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQ0NGOztBREVBO0VBQ0UsWUFBQTtFQUNBLGVBQUE7QUNDRjs7QURFQTtFQUNFLGNBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtBQ0NGOztBREVBO0VBQ0UsY0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDQ0Y7O0FERUE7RUFDRSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ0NGOztBREVBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSwyQkFBQTtBQ0NGOztBREVBO0VBQ0UsWUFBQTtFQUNBLGFBQUE7RUFDQSxlQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHFDQUFBO0FDQ0Y7O0FERUE7RUFDRSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EscUNBQUE7QUNDRjs7QURFQTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsZ0NBQUE7RUFDQSwwQ0FBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtBQ0NGOztBREVBO0VBQ0UsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FDQ0Y7O0FERUE7RUFDRSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0VBQ0EsMENBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7QUNDRjs7QURFQTtFQUNFLGdCQUFBO0FDQ0Y7O0FERUE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLDJCQUFBO0FDQ0Y7O0FERUE7RUFDRSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0FDQ0Y7O0FERUE7RUFDRSxzR0FBQTtVQUFBLDhGQUFBO0VBQ0EscUJBQUE7RUFDQSxvQ0FBQTtBQ0NGOztBREVBO0VBQ0UsOEJBQUE7QUNDRjs7QURFQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9jb250YWN0L2NvbnRhY3QuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoMntcclxuICBjb2xvcjogI2ZmZmZmZjtcclxuICBwYWRkaW5nOiAxMDBweCA7XHJcbiAgZm9udC1zaXplOiA0MHB4O1xyXG59XHJcblxyXG4uZm9ybWJveCB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxufVxyXG5cclxuLmZvcm1lbGVtZW50IHB7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxufVxyXG5cclxuLmZvcm1lbGVtZW50IHNwYW4ge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIGZvbnQtc2l6ZTogNnB4O1xyXG4gIGNvbG9yOiAjZmE4MDcyO1xyXG59XHJcblxyXG4uZm9ybWVsZW1lbnQgaW5wdXQge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiA0MDBweDtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAyNXB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDJweDtcclxuICBwYWRkaW5nOiAycHg7XHJcbn1cclxuXHJcbi5mb3JtZWxlbWVudCB0ZXh0YXJlYSB7XHJcbiAgd2lkdGg6IDQwMHB4O1xyXG4gIG1pbi1oZWlnaHQ6IDIwMHB4O1xyXG4gIGJvcmRlcjogbm9uZTtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgbGluZS1oZWlnaHQ6IDI1cHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMnB4O1xyXG4gIHBhZGRpbmc6IDJweDtcclxuICB3b3JkLWJyZWFrOiBub3JtYWw7XHJcbiAgZm9udC1mYW1pbHk6IEFyaWFsO1xyXG59XHJcblxyXG4uYnRuY29uZmlybSBidXR0b24ge1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBwYWRkaW5nOiA2cHggMzBweDtcclxuICBtYXJnaW46IDIwcHg7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICBib3JkZXI6IDJweCBzb2xpZCByb3lhbGJsdWU7XHJcbn1cclxuXHJcbiNtb2RhbGNvbmZpcm0ge1xyXG4gIHotaW5kZXg6IDk4MDtcclxuICBkaXNwbGF5OiBub25lO1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICB0b3A6IDA7XHJcbiAgbGVmdDogMDtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEyMCU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjc1KTtcclxufVxyXG5cclxuI21vZGFsc2VuZGVkIHtcclxuICB6LWluZGV4OiA5OTA7XHJcbiAgZGlzcGxheTogbm9uZTtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgdG9wOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMjAlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC43NSk7XHJcbn1cclxuXHJcbi5tb2RhbG1haW4ge1xyXG4gIHotaW5kZXg6IDk5OTtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiA0MCU7XHJcbiAgbGVmdDogNTAlO1xyXG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigyMzAsIDIzMCwgMjUwLCAwLjgpO1xyXG4gIHBhZGRpbmc6IDMwcHg7XHJcbiAgbWF4LXdpZHRoOiA0MDBweDtcclxufVxyXG5cclxuLm1vZGFsbWFpbiBwIHtcclxuICBwYWRkaW5nOiA2cHggMjBweDtcclxuICB3b3JkLXdyYXA6IG5vcm1hbDtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG59XHJcblxyXG4ubW9kYWxtYWluMiB7XHJcbiAgei1pbmRleDogOTk5O1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDQwJTtcclxuICBsZWZ0OiA1MCU7XHJcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDIzMCwgMjMwLCAyNTAsIDAuOCk7XHJcbiAgcGFkZGluZzogMzBweDtcclxuICBtYXgtd2lkdGg6IDQwMHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG5cclxuLm1vZGFsYnRue1xyXG4gIG1hcmdpbjogMCBhdXRvIDAgO1xyXG59XHJcblxyXG4ubW9kYWxidG4gYnV0dG9ue1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBwYWRkaW5nOiA2cHggMzBweDtcclxuICBtYXJnaW46IDIwcHg7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICBib3JkZXI6IDJweCBzb2xpZCByb3lhbGJsdWU7XHJcbn1cclxuXHJcbi5saW5raW5jb25ib3gge1xyXG4gIG1hcmdpbjogNTBweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbn1cclxuXHJcbi5saW5raWNvbiB7XHJcbiAgZmlsdGVyOiBpbnZlcnQoODglKSBzZXBpYSg2MSUpIHNhdHVyYXRlKDAlKSBodWUtcm90YXRlKDIyOWRlZykgYnJpZ2h0bmVzcygxMDclKSBjb250cmFzdCgxMDElKTtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgYm9yZGVyLWJvdHRvbTogc29saWQgMnB4IHRyYW5zcGFyZW50O1xyXG59XHJcblxyXG4ubGlua2ljb246aG92ZXIge1xyXG4gIGJvcmRlci1ib3R0b206IHNvbGlkIDJweCBibGFjaztcclxufVxyXG5cclxuLmxpbmtpbWcge1xyXG4gIHdpZHRoOiA1MHB4O1xyXG4gIHBhZGRpbmc6IDZweDtcclxufVxyXG4iLCJoMiB7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBwYWRkaW5nOiAxMDBweDtcbiAgZm9udC1zaXplOiA0MHB4O1xufVxuXG4uZm9ybWJveCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTZweDtcbn1cblxuLmZvcm1lbGVtZW50IHAge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cblxuLmZvcm1lbGVtZW50IHNwYW4ge1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC1zaXplOiA2cHg7XG4gIGNvbG9yOiAjZmE4MDcyO1xufVxuXG4uZm9ybWVsZW1lbnQgaW5wdXQge1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDQwMHB4O1xuICBib3JkZXI6IG5vbmU7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbGluZS1oZWlnaHQ6IDI1cHg7XG4gIG1hcmdpbi1ib3R0b206IDJweDtcbiAgcGFkZGluZzogMnB4O1xufVxuXG4uZm9ybWVsZW1lbnQgdGV4dGFyZWEge1xuICB3aWR0aDogNDAwcHg7XG4gIG1pbi1oZWlnaHQ6IDIwMHB4O1xuICBib3JkZXI6IG5vbmU7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbGluZS1oZWlnaHQ6IDI1cHg7XG4gIG1hcmdpbi1ib3R0b206IDJweDtcbiAgcGFkZGluZzogMnB4O1xuICB3b3JkLWJyZWFrOiBub3JtYWw7XG4gIGZvbnQtZmFtaWx5OiBBcmlhbDtcbn1cblxuLmJ0bmNvbmZpcm0gYnV0dG9uIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBwYWRkaW5nOiA2cHggMzBweDtcbiAgbWFyZ2luOiAyMHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgYm9yZGVyOiAycHggc29saWQgcm95YWxibHVlO1xufVxuXG4jbW9kYWxjb25maXJtIHtcbiAgei1pbmRleDogOTgwO1xuICBkaXNwbGF5OiBub25lO1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTIwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjc1KTtcbn1cblxuI21vZGFsc2VuZGVkIHtcbiAgei1pbmRleDogOTkwO1xuICBkaXNwbGF5OiBub25lO1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTIwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjc1KTtcbn1cblxuLm1vZGFsbWFpbiB7XG4gIHotaW5kZXg6IDk5OTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDQwJTtcbiAgbGVmdDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyMzAsIDIzMCwgMjUwLCAwLjgpO1xuICBwYWRkaW5nOiAzMHB4O1xuICBtYXgtd2lkdGg6IDQwMHB4O1xufVxuXG4ubW9kYWxtYWluIHAge1xuICBwYWRkaW5nOiA2cHggMjBweDtcbiAgd29yZC13cmFwOiBub3JtYWw7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG5cbi5tb2RhbG1haW4yIHtcbiAgei1pbmRleDogOTk5O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNDAlO1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDIzMCwgMjMwLCAyNTAsIDAuOCk7XG4gIHBhZGRpbmc6IDMwcHg7XG4gIG1heC13aWR0aDogNDAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbi5tb2RhbGJ0biB7XG4gIG1hcmdpbjogMCBhdXRvIDA7XG59XG5cbi5tb2RhbGJ0biBidXR0b24ge1xuICBmb250LXNpemU6IDIwcHg7XG4gIHBhZGRpbmc6IDZweCAzMHB4O1xuICBtYXJnaW46IDIwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICBib3JkZXI6IDJweCBzb2xpZCByb3lhbGJsdWU7XG59XG5cbi5saW5raW5jb25ib3gge1xuICBtYXJnaW46IDUwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG59XG5cbi5saW5raWNvbiB7XG4gIGZpbHRlcjogaW52ZXJ0KDg4JSkgc2VwaWEoNjElKSBzYXR1cmF0ZSgwJSkgaHVlLXJvdGF0ZSgyMjlkZWcpIGJyaWdodG5lc3MoMTA3JSkgY29udHJhc3QoMTAxJSk7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgYm9yZGVyLWJvdHRvbTogc29saWQgMnB4IHRyYW5zcGFyZW50O1xufVxuXG4ubGlua2ljb246aG92ZXIge1xuICBib3JkZXItYm90dG9tOiBzb2xpZCAycHggYmxhY2s7XG59XG5cbi5saW5raW1nIHtcbiAgd2lkdGg6IDUwcHg7XG4gIHBhZGRpbmc6IDZweDtcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ContactComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-contact',
                templateUrl: './contact.component.html',
                styleUrls: ['./contact.component.scss']
            }]
    }], function () { return [{ type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }, { type: _contact2_service__WEBPACK_IMPORTED_MODULE_2__["ContactService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/contact/contact2.service.ts":
/*!*********************************************!*\
  !*** ./src/app/contact/contact2.service.ts ***!
  \*********************************************/
/*! exports provided: ContactService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactService", function() { return ContactService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");




class ContactService {
    constructor(afs) {
        this.afs = afs;
    }
    addnewmail(mail) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["from"])(this.afs.collection('mailfromweb').add(mail));
    }
}
ContactService.ɵfac = function ContactService_Factory(t) { return new (t || ContactService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"])); };
ContactService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: ContactService, factory: ContactService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](ContactService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }]; }, null); })();


/***/ }),

/***/ "./src/app/experience/experience.component.ts":
/*!****************************************************!*\
  !*** ./src/app/experience/experience.component.ts ***!
  \****************************************************/
/*! exports provided: ExperienceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExperienceComponent", function() { return ExperienceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class ExperienceComponent {
    constructor() {
    }
    ngOnInit() {
    }
    ballmove() {
        var target = document.getElementById("ballerika");
        target.classList.remove('ballerikayellow');
        target.classList.add('ballerikared');
    }
}
ExperienceComponent.ɵfac = function ExperienceComponent_Factory(t) { return new (t || ExperienceComponent)(); };
ExperienceComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ExperienceComponent, selectors: [["app-experience"]], decls: 70, vars: 0, consts: [[1, "exp-container"], [1, "exp-box"], [1, "leftline"], ["id", "ballerika", 1, "ballerikayellow", 3, "click"], [1, "exp-content"], [1, "textn"], [1, "textb"], [1, "textc"]], template: function ExperienceComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "EXPERIENCES");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "section");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " DIPLOMES et FORMATION");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ExperienceComponent_Template_div_click_8_listener() { return ctx.ballmove(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "i");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "En France");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "White Rabbit \u00E0 Clichy (Formation professionnelle en codage informatique sp\u00E9cialis\u00E9 en javascript)");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "p", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "2019 - ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "span", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Aujourd'hui");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "L\u2019Institut d\u2019Enseignement Sup\u00E9rieur d\u2019Informatique et de gestion \u00E0 Paris");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "p", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "2018 - 2019");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "p", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "i");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Au Japon");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, " Licence en lettres, sciences \u00E9conomiques et politiques \u00E0 l\u2019universit\u00E9 de la pr\u00E9fecture d\u2019Aichi ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "p", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "(Parcours lettres sp\u00E9cialisation en langue fran\u00E7aise) ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "p", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "2014 - 2017");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "Scolarit\u00E9 au lyc\u00E9e Showa");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "p", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "2011 - 2014");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "section");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "BENEVOLAT et EXPERIENCES PROFESSIONNELLES");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "span", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "R\u00E9dacteur chef et membre cr\u00E9ateur du journal de l\u2019association de l\u2019universit\u00E9 de la pr\u00E9fecture d\u2019Aichi");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "p", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "(interviews, r\u00E9daction, enqu\u00EAte, collecte d\u2019informations, collecte de fonds, bases de l\u2019animation d\u2019un site internet, conception d\u2019un journal).");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "p", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "2014 \u2013 2017 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](53, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "Aide \u00E0 l\u2019utilisation des outils informatiques");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "p", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57, "(Word, Excel, Powerpoint, installation Antivirus) aux \u00E9tudiants de premi\u00E8re ann\u00E9e de l\u2019universit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "p", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "2014 \u2013 2017");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, "Animateur aux salons et Journ\u00E9es portes-ouvertes destin\u00E9s aux lyc\u00E9ens \u00E0 l\u2019universit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "p", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, "2014 - 2015");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](65, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "p", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "Animateur, pr\u00E9sentateur des jeux vid\u00E9os et robots aux enfants pour l\u2019entreprise GET");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "p", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "2012 - 2016");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: [".exp-container[_ngcontent-%COMP%] {\n  width: 720px;\n  margin: 0 auto 0;\n}\n\nh2[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 100px;\n  font-size: 40px;\n}\n\nh3[_ngcontent-%COMP%] {\n  color: #00caf4;\n  font-size: 30px;\n  margin-bottom: 30px;\n}\n\nsection[_ngcontent-%COMP%] {\n  margin: 0 auto 0;\n  text-align: left;\n}\n\n.exp-box[_ngcontent-%COMP%] {\n  margin: 0 auto 100px;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n}\n\n.exp-content[_ngcontent-%COMP%] {\n  left: -10px;\n  text-align: left;\n  width: 600px;\n}\n\n.exp-content[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  margin: 0 0 10px 60px;\n}\n\n.textn[_ngcontent-%COMP%] {\n  color: silver;\n}\n\n.textb[_ngcontent-%COMP%] {\n  color: #00caf4;\n  font-size: 20px;\n}\n\n.textc[_ngcontent-%COMP%] {\n  padding: 5px 10px;\n  background-color: yellow;\n  border-radius: 2px;\n  color: midnightblue;\n}\n\n.leftline[_ngcontent-%COMP%] {\n  width: 10px;\n  border: solid 5px white;\n  border-radius: 10px;\n  background-color: transparent;\n  z-index: 1;\n}\n\n.ballerikayellow[_ngcontent-%COMP%] {\n  height: 14px;\n  width: 14px;\n  margin: 0 0 0 -2px;\n  border-radius: 50%;\n  background-color: yellow;\n}\n\n.ballerikared[_ngcontent-%COMP%] {\n  height: 14px;\n  width: 14px;\n  margin: 400px 0 0 -2px;\n  border-radius: 50%;\n  background-color: orange;\n  transition: all 1000ms ease;\n}\n\n.triangle[_ngcontent-%COMP%] {\n  width: 30px;\n  border-top: 29px solid transparent;\n  border-bottom: 29px solid transparent;\n  border-left: 50px solid aquamarine;\n  z-index: 9;\n  position: absolute;\n  margin: -16px 0 0 -30px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZXhwZXJpZW5jZS9DOlxcVXNlcnNcXGh1XFxXZWJzdG9ybVByb2plY3RzXFxzaXRlcG9ydGZvbGlvXFxzaXRlcG9ydGZvbGlvWVQvc3JjXFxhcHBcXGV4cGVyaWVuY2VcXGV4cGVyaWVuY2UuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2V4cGVyaWVuY2UvZXhwZXJpZW5jZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFlBQUE7RUFDQSxnQkFBQTtBQ0NGOztBREVBO0VBQ0UsY0FBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDQ0Y7O0FER0E7RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FDQUY7O0FER0E7RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0FDQUY7O0FESUE7RUFDRSxvQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FDREY7O0FESUE7RUFDRSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FDREY7O0FESUE7RUFDRSxxQkFBQTtBQ0RGOztBRElBO0VBQ0UsYUFBQTtBQ0RGOztBREtBO0VBQ0UsY0FBQTtFQUNBLGVBQUE7QUNGRjs7QURLQTtFQUNFLGlCQUFBO0VBQ0Esd0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDRkY7O0FET0E7RUFDRSxXQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLDZCQUFBO0VBQ0EsVUFBQTtBQ0pGOztBRE9BO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0Esd0JBQUE7QUNKRjs7QURPQTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLHdCQUFBO0VBQ0EsMkJBQUE7QUNKRjs7QURTQTtFQUNFLFdBQUE7RUFDQSxrQ0FBQTtFQUNBLHFDQUFBO0VBQ0Esa0NBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtBQ05GIiwiZmlsZSI6InNyYy9hcHAvZXhwZXJpZW5jZS9leHBlcmllbmNlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmV4cC1jb250YWluZXIge1xyXG4gIHdpZHRoOiA3MjBweDtcclxuICBtYXJnaW46MCBhdXRvIDA7XHJcbn1cclxuXHJcbmgye1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIHBhZGRpbmc6IDEwMHB4IDtcclxuICBmb250LXNpemU6IDQwcHg7XHJcblxyXG59XHJcblxyXG5oMyB7XHJcbiAgY29sb3I6ICMwMGNhZjQ7XHJcbiAgZm9udC1zaXplOiAzMHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDMwcHg7XHJcbn1cclxuXHJcbnNlY3Rpb24ge1xyXG4gIG1hcmdpbjogMCBhdXRvIDA7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxufVxyXG5cclxuLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXHJcbi5leHAtYm94IHtcclxuICBtYXJnaW46IDAgYXV0byAxMDBweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbi5leHAtY29udGVudCB7XHJcbiAgbGVmdDogLTEwcHg7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICB3aWR0aDogNjAwcHg7XHJcbn1cclxuXHJcbi5leHAtY29udGVudCBwIHtcclxuICBtYXJnaW46IDAgMCAxMHB4IDYwcHggO1xyXG59XHJcblxyXG4udGV4dG4ge1xyXG4gIGNvbG9yOiBzaWx2ZXI7XHJcblxyXG59XHJcblxyXG4udGV4dGIge1xyXG4gIGNvbG9yOiAjMDBjYWY0O1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxufVxyXG5cclxuLnRleHRjIHtcclxuICBwYWRkaW5nOiA1cHggMTBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB5ZWxsb3c7XHJcbiAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gIGNvbG9yOiBtaWRuaWdodGJsdWU7XHJcbn1cclxuXHJcbi8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xyXG5cclxuLmxlZnRsaW5lIHtcclxuICB3aWR0aDogMTBweDtcclxuICBib3JkZXI6IHNvbGlkIDVweCB3aGl0ZTtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gIHotaW5kZXg6IDE7XHJcbn1cclxuXHJcbi5iYWxsZXJpa2F5ZWxsb3d7XHJcbiAgaGVpZ2h0OiAxNHB4O1xyXG4gIHdpZHRoOiAxNHB4O1xyXG4gIG1hcmdpbjogMCAwIDAgLTJweCA7XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHllbGxvdztcclxufVxyXG5cclxuLmJhbGxlcmlrYXJlZHtcclxuICBoZWlnaHQ6IDE0cHg7XHJcbiAgd2lkdGg6IDE0cHg7XHJcbiAgbWFyZ2luOiA0MDBweCAwIDAgLTJweDtcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogb3JhbmdlO1xyXG4gIHRyYW5zaXRpb246IGFsbCAxMDAwbXMgZWFzZTs7XHJcbn1cclxuXHJcblxyXG5cclxuLnRyaWFuZ2xlIHtcclxuICB3aWR0aDogMzBweDtcclxuICBib3JkZXItdG9wOiAyOXB4IHNvbGlkIHRyYW5zcGFyZW50O1xyXG4gIGJvcmRlci1ib3R0b206IDI5cHggc29saWQgdHJhbnNwYXJlbnQ7XHJcbiAgYm9yZGVyLWxlZnQ6IDUwcHggc29saWQgYXF1YW1hcmluZTtcclxuICB6LWluZGV4OiA5O1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBtYXJnaW46IC0xNnB4IDAgMCAtMzBweDtcclxuXHJcbn1cclxuXHJcblxyXG5cclxuIiwiLmV4cC1jb250YWluZXIge1xuICB3aWR0aDogNzIwcHg7XG4gIG1hcmdpbjogMCBhdXRvIDA7XG59XG5cbmgyIHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHBhZGRpbmc6IDEwMHB4O1xuICBmb250LXNpemU6IDQwcHg7XG59XG5cbmgzIHtcbiAgY29sb3I6ICMwMGNhZjQ7XG4gIGZvbnQtc2l6ZTogMzBweDtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cblxuc2VjdGlvbiB7XG4gIG1hcmdpbjogMCBhdXRvIDA7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG5cbi5leHAtYm94IHtcbiAgbWFyZ2luOiAwIGF1dG8gMTAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4uZXhwLWNvbnRlbnQge1xuICBsZWZ0OiAtMTBweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgd2lkdGg6IDYwMHB4O1xufVxuXG4uZXhwLWNvbnRlbnQgcCB7XG4gIG1hcmdpbjogMCAwIDEwcHggNjBweDtcbn1cblxuLnRleHRuIHtcbiAgY29sb3I6IHNpbHZlcjtcbn1cblxuLnRleHRiIHtcbiAgY29sb3I6ICMwMGNhZjQ7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cblxuLnRleHRjIHtcbiAgcGFkZGluZzogNXB4IDEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHllbGxvdztcbiAgYm9yZGVyLXJhZGl1czogMnB4O1xuICBjb2xvcjogbWlkbmlnaHRibHVlO1xufVxuXG4ubGVmdGxpbmUge1xuICB3aWR0aDogMTBweDtcbiAgYm9yZGVyOiBzb2xpZCA1cHggd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICB6LWluZGV4OiAxO1xufVxuXG4uYmFsbGVyaWtheWVsbG93IHtcbiAgaGVpZ2h0OiAxNHB4O1xuICB3aWR0aDogMTRweDtcbiAgbWFyZ2luOiAwIDAgMCAtMnB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQtY29sb3I6IHllbGxvdztcbn1cblxuLmJhbGxlcmlrYXJlZCB7XG4gIGhlaWdodDogMTRweDtcbiAgd2lkdGg6IDE0cHg7XG4gIG1hcmdpbjogNDAwcHggMCAwIC0ycHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogb3JhbmdlO1xuICB0cmFuc2l0aW9uOiBhbGwgMTAwMG1zIGVhc2U7XG59XG5cbi50cmlhbmdsZSB7XG4gIHdpZHRoOiAzMHB4O1xuICBib3JkZXItdG9wOiAyOXB4IHNvbGlkIHRyYW5zcGFyZW50O1xuICBib3JkZXItYm90dG9tOiAyOXB4IHNvbGlkIHRyYW5zcGFyZW50O1xuICBib3JkZXItbGVmdDogNTBweCBzb2xpZCBhcXVhbWFyaW5lO1xuICB6LWluZGV4OiA5O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbjogLTE2cHggMCAwIC0zMHB4O1xufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ExperienceComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-experience',
                templateUrl: './experience.component.html',
                styleUrls: ['./experience.component.scss']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class HomeComponent {
    constructor() { }
    ngOnInit() {
    }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) { return new (t || HomeComponent)(); };
HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeComponent, selectors: [["app-home"]], decls: 26, vars: 0, consts: [[1, "home-box"], [1, "portrait"], [1, "texto"]], template: function HomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "section");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Bonjour !");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Je m\u2019appelle Takahiro (surnomm\u00E9 par mes coll\u00E8gues \u00AB TAKA \u00BB), j'ai 24 ans.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Je suis en formation pour r\u00E9aliser mon r\u00EAve : devenir d\u00E9veloppeur web en France !");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Mon parcours, \u00E9tape 1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Au cours de mon cursus universitaire au Japon, dans la ville de Nagoya, j\u2019ai eu l\u2019opportunit\u00E9 de faire une ann\u00E9e de c\u00E9sure dans un des pays partenaires qui m\u2019attirait le plus. Mon choix a \u00E9t\u00E9 tr\u00E8s rapide car je connaissais d\u00E9j\u00E0 la destination de mes r\u00EAves. J\u2019ai donc fait le choix de venir en France durant un an avec le fameux PVT.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "J\u2019ai int\u00E9gr\u00E9 une \u00E9cole fran\u00E7aise");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, " o\u00F9 je prenais \u00E0 la fois des cours de fran\u00E7ais pour m\u2019am\u00E9liorer (surtout \u00E0 l\u2019oral) et \u00E0 la fois des cours d\u2019informatique, mes deux projets qui me tenaient \u00E0 c\u0153ur. En plus d\u2019avoir fait une merveilleuse rencontre, j\u2019ai d\u00E9velopp\u00E9 l\u2019envie de construire mon projet de travailler dans le web en France.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Et maintenant ? \u00E9tape 2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Je suis install\u00E9 \u00E0 la Garenne-Colombes (92), et comme je souhaitais apprendre vite, de fa\u00E7on autonome et efficace les bases du codage informatique, j\u2019ai opt\u00E9 pour une formation professionnalisante. J'ai choisi ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "White Rabbit, une \u00E9cole labellis\u00E9e Grande Ecole du Num\u00E9rique");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, ", une p\u00E9dagogie bas\u00E9e sur les neurosciences et la m\u00E9thode Montessori, pour acqu\u00E9rir les bases n\u00E9cessaires pour l\u2019apprentissage du code et la r\u00E9alisation d\u2019un site web.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Et l\u00E0, je suis au bout de la formation th\u00E9orique, et me lance dans la formation pratique gr\u00E2ce \u00E0 un \u00E9ventuel stage de fin d'ann\u00E9e. J\u2019esp\u00E8re que ce site appuiera ma volont\u00E9 de concr\u00E9tiser mon r\u00EAve avec de futurs collaborateurs.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: [".home-box[_ngcontent-%COMP%] {\n  margin: 50px auto 50px;\n  width: 720px;\n  height: 100%;\n  text-align: left;\n  color: #FFFFFF;\n}\n\nh3[_ngcontent-%COMP%] {\n  margin: 20px 0;\n  font-size: 50px;\n}\n\nh4[_ngcontent-%COMP%] {\n  margin: 40px 0 10px 0;\n  font-size: 28px;\n}\n\n.texto[_ngcontent-%COMP%] {\n  font-size: 20px;\n}\n\n.home-box[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  margin-left: 20px;\n  line-height: 30px;\n}\n\n.home-box[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  color: #00caf4;\n}\n\n.portrait[_ngcontent-%COMP%] {\n  margin: 0 auto 30px;\n  height: 250px;\n  width: 250px;\n  position: relative;\n  background-image: url('0022.jpg');\n  border-radius: 50%;\n  background-size: cover;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9DOlxcVXNlcnNcXGh1XFxXZWJzdG9ybVByb2plY3RzXFxzaXRlcG9ydGZvbGlvXFxzaXRlcG9ydGZvbGlvWVQvc3JjXFxhcHBcXGhvbWVcXGhvbWUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUNDRjs7QURFQTtFQUNFLGNBQUE7RUFDQSxlQUFBO0FDQ0Y7O0FERUE7RUFDRSxxQkFBQTtFQUNBLGVBQUE7QUNDRjs7QURFQTtFQUNFLGVBQUE7QUNDRjs7QURHQTtFQUNFLGlCQUFBO0VBQ0EsaUJBQUE7QUNBRjs7QURHQTtFQUNFLGNBQUE7QUNBRjs7QURHQTtFQUNFLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGlDQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtBQ0FGIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhvbWUtYm94IHtcclxuICBtYXJnaW46IDUwcHggYXV0byA1MHB4O1xyXG4gIHdpZHRoOiA3MjBweDtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICBjb2xvcjogI0ZGRkZGRjtcclxufVxyXG5cclxuaDMge1xyXG4gIG1hcmdpbjogMjBweCAwO1xyXG4gIGZvbnQtc2l6ZTogNTBweDtcclxufVxyXG5cclxuaDQge1xyXG4gIG1hcmdpbjogNDBweCAwIDEwcHggMDtcclxuICBmb250LXNpemU6IDI4cHg7XHJcbn1cclxuXHJcbi50ZXh0byB7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG5cclxufVxyXG5cclxuLmhvbWUtYm94IHAge1xyXG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAzMHB4O1xyXG59XHJcblxyXG4uaG9tZS1ib3ggc3BhbiB7XHJcbiAgY29sb3I6ICMwMGNhZjQ7XHJcbn1cclxuXHJcbi5wb3J0cmFpdCB7XHJcbiAgbWFyZ2luOiAwIGF1dG8gMzBweDtcclxuICBoZWlnaHQ6IDI1MHB4O1xyXG4gIHdpZHRoOiAyNTBweDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vYXNzZXRzLzAwMjIuanBnXCIpO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG59XHJcblxyXG4iLCIuaG9tZS1ib3gge1xuICBtYXJnaW46IDUwcHggYXV0byA1MHB4O1xuICB3aWR0aDogNzIwcHg7XG4gIGhlaWdodDogMTAwJTtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgY29sb3I6ICNGRkZGRkY7XG59XG5cbmgzIHtcbiAgbWFyZ2luOiAyMHB4IDA7XG4gIGZvbnQtc2l6ZTogNTBweDtcbn1cblxuaDQge1xuICBtYXJnaW46IDQwcHggMCAxMHB4IDA7XG4gIGZvbnQtc2l6ZTogMjhweDtcbn1cblxuLnRleHRvIHtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuXG4uaG9tZS1ib3ggcCB7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xuICBsaW5lLWhlaWdodDogMzBweDtcbn1cblxuLmhvbWUtYm94IHNwYW4ge1xuICBjb2xvcjogIzAwY2FmNDtcbn1cblxuLnBvcnRyYWl0IHtcbiAgbWFyZ2luOiAwIGF1dG8gMzBweDtcbiAgaGVpZ2h0OiAyNTBweDtcbiAgd2lkdGg6IDI1MHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uL2Fzc2V0cy8wMDIyLmpwZ1wiKTtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HomeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-home',
                templateUrl: './home.component.html',
                styleUrls: ['./home.component.scss']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/loisir/loisir.component.ts":
/*!********************************************!*\
  !*** ./src/app/loisir/loisir.component.ts ***!
  \********************************************/
/*! exports provided: LoisirComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoisirComponent", function() { return LoisirComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class LoisirComponent {
    constructor() { }
    ngOnInit() {
    }
}
LoisirComponent.ɵfac = function LoisirComponent_Factory(t) { return new (t || LoisirComponent)(); };
LoisirComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: LoisirComponent, selectors: [["app-loisir"]], decls: 20, vars: 0, consts: [[1, "loisirbox"], [1, "loisiritema"], [1, "mask"], [1, "caption"], [1, "loisiritemc"], [1, "loisiritemb"], [1, "loisiritemd"]], template: function LoisirComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "LOISIR");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Rugby");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Break Dance");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Calligraphie japonaise");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Cuisine");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["h2[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 100px;\n  font-size: 40px;\n}\n\n.loisirbox[_ngcontent-%COMP%] {\n  margin: 0 auto 200px;\n  width: 720px;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  flex-wrap: wrap;\n}\n\n.loisiritema[_ngcontent-%COMP%] {\n  height: 250px;\n  width: 250px;\n  background-image: url('loisir1.png');\n  background-size: cover;\n  margin: 40px;\n  position: relative;\n}\n\n.loisiritema[_ngcontent-%COMP%]   .mask[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  top: 0;\n  left: 0;\n  opacity: 0;\n  background-color: rgba(0, 0, 0, 0.6);\n  transition: all 0.2s ease;\n}\n\n.loisiritema[_ngcontent-%COMP%]:hover   .mask[_ngcontent-%COMP%] {\n  opacity: 1;\n}\n\n.loisiritemb[_ngcontent-%COMP%] {\n  height: 250px;\n  width: 250px;\n  background-image: url('loisir2.jpg');\n  background-size: cover;\n  margin: 40px;\n  position: relative;\n}\n\n.loisiritemb[_ngcontent-%COMP%]   .mask[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  top: 0;\n  left: 0;\n  opacity: 0;\n  background-color: rgba(0, 0, 0, 0.6);\n  transition: all 0.2s ease;\n}\n\n.loisiritemb[_ngcontent-%COMP%]:hover   .mask[_ngcontent-%COMP%] {\n  opacity: 1;\n}\n\n.loisiritemc[_ngcontent-%COMP%] {\n  height: 250px;\n  width: 250px;\n  background-image: url('loisir3.png');\n  background-size: cover;\n  margin: 40px;\n  position: relative;\n}\n\n.loisiritemc[_ngcontent-%COMP%]   .mask[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  top: 0;\n  left: 0;\n  opacity: 0;\n  background-color: rgba(0, 0, 0, 0.6);\n  transition: all 0.2s ease;\n}\n\n.loisiritemc[_ngcontent-%COMP%]:hover   .mask[_ngcontent-%COMP%] {\n  opacity: 1;\n}\n\n.loisiritemd[_ngcontent-%COMP%] {\n  height: 250px;\n  width: 250px;\n  background-image: url('loisir4.jpg');\n  background-size: cover;\n  margin: 40px;\n  position: relative;\n}\n\n.loisiritemd[_ngcontent-%COMP%]   .mask[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  top: 0;\n  left: 0;\n  opacity: 0;\n  background-color: rgba(0, 0, 0, 0.6);\n  transition: all 0.2s ease;\n}\n\n.loisiritemd[_ngcontent-%COMP%]:hover   .mask[_ngcontent-%COMP%] {\n  opacity: 1;\n}\n\n.caption[_ngcontent-%COMP%] {\n  margin-top: 110px;\n  font-size: 130%;\n  text-align: center;\n  color: #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9pc2lyL0M6XFxVc2Vyc1xcaHVcXFdlYnN0b3JtUHJvamVjdHNcXHNpdGVwb3J0Zm9saW9cXHNpdGVwb3J0Zm9saW9ZVC9zcmNcXGFwcFxcbG9pc2lyXFxsb2lzaXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xvaXNpci9sb2lzaXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxjQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUNDRjs7QURFQTtFQUNFLG9CQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtBQ0NGOztBREVBO0VBQ0UsYUFBQTtFQUNBLFlBQUE7RUFDQSxvQ0FBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDQ0Y7O0FEQ0E7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxVQUFBO0VBQ0Esb0NBQUE7RUFFQSx5QkFBQTtBQ0VGOztBREFBO0VBQ0UsVUFBQTtBQ0dGOztBREFBO0VBQ0UsYUFBQTtFQUNBLFlBQUE7RUFDQSxvQ0FBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDR0Y7O0FEREE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxVQUFBO0VBQ0Esb0NBQUE7RUFFQSx5QkFBQTtBQ0lGOztBREZBO0VBQ0UsVUFBQTtBQ0tGOztBREZBO0VBQ0UsYUFBQTtFQUNBLFlBQUE7RUFDQSxvQ0FBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDS0Y7O0FESEE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxVQUFBO0VBQ0Esb0NBQUE7RUFFQSx5QkFBQTtBQ01GOztBREpBO0VBQ0UsVUFBQTtBQ09GOztBREpBO0VBQ0UsYUFBQTtFQUNBLFlBQUE7RUFDQSxvQ0FBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDT0Y7O0FETEE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxVQUFBO0VBQ0Esb0NBQUE7RUFFQSx5QkFBQTtBQ1FGOztBRE5BO0VBQ0UsVUFBQTtBQ1NGOztBRE5BO0VBQ0UsaUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDU0YiLCJmaWxlIjoic3JjL2FwcC9sb2lzaXIvbG9pc2lyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaDIge1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIHBhZGRpbmc6IDEwMHB4O1xyXG4gIGZvbnQtc2l6ZTogNDBweDtcclxufVxyXG5cclxuLmxvaXNpcmJveCB7XHJcbiAgbWFyZ2luOiAwIGF1dG8gMjAwcHg7XHJcbiAgd2lkdGg6IDcyMHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBmbGV4LXdyYXA6IHdyYXA7XHJcbn1cclxuXHJcbi5sb2lzaXJpdGVtYSB7XHJcbiAgaGVpZ2h0OiAyNTBweDtcclxuICB3aWR0aDogMjUwcHg7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vYXNzZXRzL2xvaXNpcjEucG5nXCIpO1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgbWFyZ2luOiA0MHB4O1xyXG4gIHBvc2l0aW9uOlx0XHRyZWxhdGl2ZTtcclxufVxyXG4ubG9pc2lyaXRlbWEgLm1hc2sge1xyXG4gIHdpZHRoOlx0XHRcdDEwMCU7XHJcbiAgaGVpZ2h0Olx0XHRcdDEwMCU7XHJcbiAgcG9zaXRpb246XHRcdGFic29sdXRlO1xyXG4gIHRvcDpcdFx0XHQwO1xyXG4gIGxlZnQ6XHRcdFx0MDtcclxuICBvcGFjaXR5Olx0XHQwO1xyXG4gIGJhY2tncm91bmQtY29sb3I6XHRyZ2JhKDAsMCwwLDAuNik7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOlx0YWxsIDAuMnMgZWFzZTtcclxuICB0cmFuc2l0aW9uOlx0XHRhbGwgMC4ycyBlYXNlO1xyXG59XHJcbi5sb2lzaXJpdGVtYTpob3ZlciAubWFzayB7XHJcbiAgb3BhY2l0eTpcdFx0MTtcclxufVxyXG5cclxuLmxvaXNpcml0ZW1iIHtcclxuICBoZWlnaHQ6IDI1MHB4O1xyXG4gIHdpZHRoOiAyNTBweDtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvbG9pc2lyMi5qcGdcIik7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICBtYXJnaW46IDQwcHg7XHJcbiAgcG9zaXRpb246XHRcdHJlbGF0aXZlO1xyXG59XHJcbi5sb2lzaXJpdGVtYiAubWFzayB7XHJcbiAgd2lkdGg6XHRcdFx0MTAwJTtcclxuICBoZWlnaHQ6XHRcdFx0MTAwJTtcclxuICBwb3NpdGlvbjpcdFx0YWJzb2x1dGU7XHJcbiAgdG9wOlx0XHRcdDA7XHJcbiAgbGVmdDpcdFx0XHQwO1xyXG4gIG9wYWNpdHk6XHRcdDA7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjpcdHJnYmEoMCwwLDAsMC42KTtcclxuICAtd2Via2l0LXRyYW5zaXRpb246XHRhbGwgMC4ycyBlYXNlO1xyXG4gIHRyYW5zaXRpb246XHRcdGFsbCAwLjJzIGVhc2U7XHJcbn1cclxuLmxvaXNpcml0ZW1iOmhvdmVyIC5tYXNrIHtcclxuICBvcGFjaXR5Olx0XHQxO1xyXG59XHJcblxyXG4ubG9pc2lyaXRlbWMge1xyXG4gIGhlaWdodDogMjUwcHg7XHJcbiAgd2lkdGg6IDI1MHB4O1xyXG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uL2Fzc2V0cy9sb2lzaXIzLnBuZ1wiKTtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gIG1hcmdpbjogNDBweDtcclxuICBwb3NpdGlvbjpcdFx0cmVsYXRpdmU7XHJcbn1cclxuLmxvaXNpcml0ZW1jIC5tYXNrIHtcclxuICB3aWR0aDpcdFx0XHQxMDAlO1xyXG4gIGhlaWdodDpcdFx0XHQxMDAlO1xyXG4gIHBvc2l0aW9uOlx0XHRhYnNvbHV0ZTtcclxuICB0b3A6XHRcdFx0MDtcclxuICBsZWZ0Olx0XHRcdDA7XHJcbiAgb3BhY2l0eTpcdFx0MDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOlx0cmdiYSgwLDAsMCwwLjYpO1xyXG4gIC13ZWJraXQtdHJhbnNpdGlvbjpcdGFsbCAwLjJzIGVhc2U7XHJcbiAgdHJhbnNpdGlvbjpcdFx0YWxsIDAuMnMgZWFzZTtcclxufVxyXG4ubG9pc2lyaXRlbWM6aG92ZXIgLm1hc2sge1xyXG4gIG9wYWNpdHk6XHRcdDE7XHJcbn1cclxuXHJcbi5sb2lzaXJpdGVtZCB7XHJcbiAgaGVpZ2h0OiAyNTBweDtcclxuICB3aWR0aDogMjUwcHg7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vYXNzZXRzL2xvaXNpcjQuanBnXCIpO1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgbWFyZ2luOiA0MHB4O1xyXG4gIHBvc2l0aW9uOlx0XHRyZWxhdGl2ZTtcclxufVxyXG4ubG9pc2lyaXRlbWQgLm1hc2sge1xyXG4gIHdpZHRoOlx0XHRcdDEwMCU7XHJcbiAgaGVpZ2h0Olx0XHRcdDEwMCU7XHJcbiAgcG9zaXRpb246XHRcdGFic29sdXRlO1xyXG4gIHRvcDpcdFx0XHQwO1xyXG4gIGxlZnQ6XHRcdFx0MDtcclxuICBvcGFjaXR5Olx0XHQwO1xyXG4gIGJhY2tncm91bmQtY29sb3I6XHRyZ2JhKDAsMCwwLDAuNik7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOlx0YWxsIDAuMnMgZWFzZTtcclxuICB0cmFuc2l0aW9uOlx0XHRhbGwgMC4ycyBlYXNlO1xyXG59XHJcbi5sb2lzaXJpdGVtZDpob3ZlciAubWFzayB7XHJcbiAgb3BhY2l0eTpcdFx0MTtcclxufVxyXG5cclxuLmNhcHRpb24ge1xyXG4gIG1hcmdpbi10b3A6IDExMHB4O1xyXG4gIGZvbnQtc2l6ZTpcdFx0MTMwJTtcclxuICB0ZXh0LWFsaWduOiBcdFx0Y2VudGVyO1xyXG4gIGNvbG9yOlx0XHRcdCNmZmY7XHJcbn1cclxuIiwiaDIge1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgcGFkZGluZzogMTAwcHg7XG4gIGZvbnQtc2l6ZTogNDBweDtcbn1cblxuLmxvaXNpcmJveCB7XG4gIG1hcmdpbjogMCBhdXRvIDIwMHB4O1xuICB3aWR0aDogNzIwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBmbGV4LXdyYXA6IHdyYXA7XG59XG5cbi5sb2lzaXJpdGVtYSB7XG4gIGhlaWdodDogMjUwcHg7XG4gIHdpZHRoOiAyNTBweDtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vYXNzZXRzL2xvaXNpcjEucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBtYXJnaW46IDQwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmxvaXNpcml0ZW1hIC5tYXNrIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIG9wYWNpdHk6IDA7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC42KTtcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlO1xufVxuXG4ubG9pc2lyaXRlbWE6aG92ZXIgLm1hc2sge1xuICBvcGFjaXR5OiAxO1xufVxuXG4ubG9pc2lyaXRlbWIge1xuICBoZWlnaHQ6IDI1MHB4O1xuICB3aWR0aDogMjUwcHg7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uL2Fzc2V0cy9sb2lzaXIyLmpwZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgbWFyZ2luOiA0MHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5sb2lzaXJpdGVtYiAubWFzayB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICBvcGFjaXR5OiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNik7XG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZTtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZTtcbn1cblxuLmxvaXNpcml0ZW1iOmhvdmVyIC5tYXNrIHtcbiAgb3BhY2l0eTogMTtcbn1cblxuLmxvaXNpcml0ZW1jIHtcbiAgaGVpZ2h0OiAyNTBweDtcbiAgd2lkdGg6IDI1MHB4O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvbG9pc2lyMy5wbmdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIG1hcmdpbjogNDBweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4ubG9pc2lyaXRlbWMgLm1hc2sge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgb3BhY2l0eTogMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjYpO1xuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2U7XG4gIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2U7XG59XG5cbi5sb2lzaXJpdGVtYzpob3ZlciAubWFzayB7XG4gIG9wYWNpdHk6IDE7XG59XG5cbi5sb2lzaXJpdGVtZCB7XG4gIGhlaWdodDogMjUwcHg7XG4gIHdpZHRoOiAyNTBweDtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vYXNzZXRzL2xvaXNpcjQuanBnXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBtYXJnaW46IDQwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmxvaXNpcml0ZW1kIC5tYXNrIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIG9wYWNpdHk6IDA7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC42KTtcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBlYXNlO1xufVxuXG4ubG9pc2lyaXRlbWQ6aG92ZXIgLm1hc2sge1xuICBvcGFjaXR5OiAxO1xufVxuXG4uY2FwdGlvbiB7XG4gIG1hcmdpbi10b3A6IDExMHB4O1xuICBmb250LXNpemU6IDEzMCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICNmZmY7XG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LoisirComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-loisir',
                templateUrl: './loisir.component.html',
                styleUrls: ['./loisir.component.scss']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/pagedev/pagedev.component.ts":
/*!**********************************************!*\
  !*** ./src/app/pagedev/pagedev.component.ts ***!
  \**********************************************/
/*! exports provided: PagedevComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagedevComponent", function() { return PagedevComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");




function PagedevComponent_tr_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r1 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r1.name);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r1.email);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r1.object);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r1.body);
} }
class PagedevComponent {
    constructor(db) {
        this.mails = db.collection('mailfromweb').valueChanges();
    }
    ngOnInit() {
    }
}
PagedevComponent.ɵfac = function PagedevComponent_Factory(t) { return new (t || PagedevComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"])); };
PagedevComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PagedevComponent, selectors: [["app-pagedev"]], decls: 12, vars: 3, consts: [[4, "ngFor", "ngForOf"], [1, "left"]], template: function PagedevComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "table");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "From");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Email");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "title");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "message");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, PagedevComponent_tr_10_Template, 9, 4, "tr", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](11, "async");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](11, 1, ctx.mails));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"]], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["AsyncPipe"]], styles: ["table[_ngcontent-%COMP%] {\n  padding: 50px 200px;\n}\n\ntr[_ngcontent-%COMP%] {\n  background-color: #f3f3f3;\n}\n\nth[_ngcontent-%COMP%], td[_ngcontent-%COMP%] {\n  padding: 10px 20px;\n  border: solid 2px royalblue;\n  vertical-align: top;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZWRldi9DOlxcVXNlcnNcXGh1XFxXZWJzdG9ybVByb2plY3RzXFxzaXRlcG9ydGZvbGlvXFxzaXRlcG9ydGZvbGlvWVQvc3JjXFxhcHBcXHBhZ2VkZXZcXHBhZ2VkZXYuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VkZXYvcGFnZWRldi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG1CQUFBO0FDQ0Y7O0FERUE7RUFDRSx5QkFBQTtBQ0NGOztBREVBO0VBQ0Usa0JBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VkZXYvcGFnZWRldi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlIHtcclxuICBwYWRkaW5nOiA1MHB4IDIwMHB4O1xyXG59XHJcblxyXG50ciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YzZjNmMztcclxufVxyXG5cclxudGgsIHRkIHtcclxuICBwYWRkaW5nOiAxMHB4IDIwcHg7XHJcbiAgYm9yZGVyOiBzb2xpZCAycHggcm95YWxibHVlO1xyXG4gIHZlcnRpY2FsLWFsaWduOiB0b3A7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxufVxyXG5cclxuXHJcbiIsInRhYmxlIHtcbiAgcGFkZGluZzogNTBweCAyMDBweDtcbn1cblxudHIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjNmM2YzO1xufVxuXG50aCwgdGQge1xuICBwYWRkaW5nOiAxMHB4IDIwcHg7XG4gIGJvcmRlcjogc29saWQgMnB4IHJveWFsYmx1ZTtcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PagedevComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-pagedev',
                templateUrl: './pagedev.component.html',
                styleUrls: ['./pagedev.component.scss']
            }]
    }], function () { return [{ type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"] }]; }, null); })();


/***/ }),

/***/ "./src/app/portfolio/portfolio.component.ts":
/*!**************************************************!*\
  !*** ./src/app/portfolio/portfolio.component.ts ***!
  \**************************************************/
/*! exports provided: PortfolioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PortfolioComponent", function() { return PortfolioComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class PortfolioComponent {
    constructor() { }
    ngOnInit() {
    }
}
PortfolioComponent.ɵfac = function PortfolioComponent_Factory(t) { return new (t || PortfolioComponent)(); };
PortfolioComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PortfolioComponent, selectors: [["app-portfolio"]], decls: 3, vars: 0, consts: [[1, "portfoliocontainer"]], template: function PortfolioComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Mes PORTFOLIOs...");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["h2[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 100px;\n  font-size: 40px;\n}\n\n.portfoliocontainer[_ngcontent-%COMP%] {\n  margin: 0 auto 0;\n  width: 720px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcG9ydGZvbGlvL0M6XFxVc2Vyc1xcaHVcXFdlYnN0b3JtUHJvamVjdHNcXHNpdGVwb3J0Zm9saW9cXHNpdGVwb3J0Zm9saW9ZVC9zcmNcXGFwcFxccG9ydGZvbGlvXFxwb3J0Zm9saW8uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BvcnRmb2xpby9wb3J0Zm9saW8uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxjQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUNDRjs7QURJQTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtBQ0RGIiwiZmlsZSI6InNyYy9hcHAvcG9ydGZvbGlvL3BvcnRmb2xpby5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImgye1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIHBhZGRpbmc6IDEwMHB4IDtcclxuICBmb250LXNpemU6IDQwcHg7XHJcbn1cclxuXHJcblxyXG5cclxuLnBvcnRmb2xpb2NvbnRhaW5lciB7XHJcbiAgbWFyZ2luOiAwIGF1dG8gMDtcclxuICB3aWR0aDogNzIwcHg7XHJcbn1cclxuXHJcbiIsImgyIHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHBhZGRpbmc6IDEwMHB4O1xuICBmb250LXNpemU6IDQwcHg7XG59XG5cbi5wb3J0Zm9saW9jb250YWluZXIge1xuICBtYXJnaW46IDAgYXV0byAwO1xuICB3aWR0aDogNzIwcHg7XG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PortfolioComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-portfolio',
                templateUrl: './portfolio.component.html',
                styleUrls: ['./portfolio.component.scss']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    firebase: {
        apiKey: "AIzaSyD-muTcMYYnv4Ehx5VxvDzydWhsZMSsO3o",
        authDomain: "sitemycv2020.firebaseapp.com",
        databaseURL: "https://sitemycv2020.firebaseio.com",
        projectId: "sitemycv2020",
        storageBucket: "sitemycv2020.appspot.com",
        messagingSenderId: "265636062200",
        appId: "1:265636062200:web:c84f0c22841fe248976c0d",
        measurementId: "G-0N6DPWK1E4"
    }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\hu\WebstormProjects\siteportfolio\siteportfolioYT\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map